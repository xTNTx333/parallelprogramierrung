package parallelProgramierrung;
/**
 * Status in den sich ein Knoten befinden kann
 */
public enum ElectionState {
	/**
	 * Der Knoten muss sich einen Leader w�hlen
	 */
	Election,
	
	/**
	 * Der Knoten hat einen Leader und kann seinen Algorithmus starten
	 */
	Leader,
	
	/**
	 * Der Knoten wartet auf den Graph der Zur�ckgegeben wird
	 */
	Graph
}
