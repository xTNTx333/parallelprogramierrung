package parallelProgramierrung;

import java.io.File;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;

/**
 * @author Felix Bruns
 * Klasse mit Hilfsfunktionen
 */
public class Helper {
	private static Logger log = Logger.getLogger(Helper.class);
	/**
	 * @param file XML Document was eingelesen werden soll
	 * @return Liefert das eingelesene XML Document als XML zur�ck
	 */
	public static Graphen getGraphen(File file) {
		try {
			JAXBContext jc = JAXBContext.newInstance(Graphen.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			SchemaFactory sf = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			URL schemaURL = Helper.class.getClassLoader().getResource("Grafen.xsd");
			Schema schema = (Schema) sf.newSchema(schemaURL);
			unmarshaller.setSchema(schema);
			return (Graphen) unmarshaller.unmarshal(file);
		} catch (Exception ex) {
			log.error("Fehler beim einlesen der XML Datei",ex);
			return null;
		}
	}

	/**
	 * Speichert den �bergebenen Graphen in ein XML Datei
	 * @param graphen Graph der Gespeichert werden soll
	 * @param file Pfad zur Datei
	 */
	public static void SaveGraphen(Graphen graphen, File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Graphen.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			if (file.exists()) {
				file.delete();
			}

			jaxbMarshaller.marshal(graphen, file);
		} catch (JAXBException e) {
			log.error("Fehler beim Speichern des Grafen", e);
		}
	}
}
