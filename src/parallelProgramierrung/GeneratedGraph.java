package parallelProgramierrung;

import gui.GraphCompletedEvent;
import gui.IGraphCompletedListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jcsp.lang.AltingChannelInput;
import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.lang.Parallel;
import org.jcsp.net.NetChannelInput;
import org.jcsp.net.NetChannelOutput;
import org.jcsp.net.NodeInitFailedException;
import org.jcsp.net.cns.CNS;
/**
 * Diese Klasse baut den Graphen auf und ist zum Starten und Stoppen der Algorithmen gedacht.
 */
public class GeneratedGraph implements CSProcess {

	/**
	 * Knoten die in dieser Instance existieren
	 */
	private HashMap<Integer, IKnoten> nodes = new HashMap<Integer, IKnoten>();
	
	/**
	 * Der Channel �ber den die Ergebnisse zur�cklaufen.
	 */
	private AltingChannelInput inResultChannel;

	/**
	 * Logger zum loggen von Fehlern
	 */
	private Logger log = Logger.getLogger(getClass());

	/**
	 * Kontroll Channel zur kontrolle der Knoten
	 */
	private List<NetChannelOutput> controllingChannel = new LinkedList<NetChannelOutput>();
	
	/**
	 * Liste mit den registrierten Listenern.
	 */
	private List<IGraphCompletedListener> listeners = new ArrayList<IGraphCompletedListener>();

	/**
	 * Damit alle Knoten gleichzeitig starten
	 */
	private Parallel par = new Parallel();
	
	/**
	 * Welcher Algorytmus leuft
	 */
	private Aufgabe aufgabe;
	
	/**
	 * Der Laufende Thread um Nachrichten an die GUI zu senden
	 */
	private volatile boolean thread;
	
	/**
	 * Klasse Generiert automatisch diesen Graph
	 * 
	 * @param graph
	 *            Der Graph der Erstellt werden soll
	 * @param pc Der pc der die Graphen starten will
	 * @param aufgabe Der Algorythmus welcher gestartet werden soll
	 * @param startNode 0 = Der Knoten welcher starten soll, 1 = Zeit zwischen den Nachrichten, 2 = Wartezeit des Timers
	 * @throws Exception
	 *             Das kann Passieren wenn der Graph nicht aufl�sbare
	 *             Referenzen hat
	 */
	public GeneratedGraph(Graphen graph, int pc, Aufgabe aufgabe,
			Integer... startNode) {
		//Der haupt Pc erstellt bekommt den Channel wo der Graph eingeht
		if (pc == 0) {
			inResultChannel = CNS.createNet2One("syncChannel");
		}
		
		this.aufgabe = aufgabe;
		par.addProcess(this);
		//Die einzelnen Knoten erstellen
		for (Graphen.Knoten node : graph.getKnoten()) {
			//Nur die Knoten erstellen die fuer den Eigenden PC sind
			if (pc == node.pc.intValue()) {
				int nodeId = node.id.intValue();
				parallelProgramierrung.IKnoten internNode = null;
				//Knoten je nach Algorytmus einstellen
				switch (aufgabe) {
				case echoAufgabe1:
					internNode = new parallelProgramierrung.Echo2(nodeId,
							CNS.createAny2Net("syncChannel"),
							CNS.createNet2One("Controling" + nodeId));
					if (startNode != null && startNode.length > 0) {
						internNode.setStartNode(startNode[0]);
					}
					break;
				case electionAufgabe1:
					internNode = new parallelProgramierrung.Echo2(nodeId,
							CNS.createAny2Net("syncChannel"),
							CNS.createNet2One("Controling" + nodeId));
					internNode.setStartNode(nodeId);
					break;
				case electionAufgabe3:
					internNode = new parallelProgramierrung.ElectionFelix(nodeId,
							CNS.createAny2Net("syncChannel"),
							CNS.createNet2One("Controling" + nodeId));
					internNode.setStartNode(nodeId);
				}
				
				//Wartezeit zwischen den Nachrichten setzen
				if(startNode != null && startNode.length > 1){
					internNode.setMessageTime(startNode[1]);
				}
				
				//Wartezeit bis eine neuen durchlauf versucht wird
				if(startNode != null && startNode.length > 2){
					internNode.setWaitForReset(startNode[2]);
				}
				
				controllingChannel
						.add(CNS.createOne2Net("Controling" + nodeId));
				nodes.put(nodeId, internNode);
				par.addProcess(internNode);
				//ChannelInput ersellen 
				for (Graphen.Knoten.Kante kante : node.getKante()) {
					int key = kante.value.intValue();
					int kantekey = key;
					if (key > nodeId) {
						key = key * 100 + nodeId;

						NetChannelInput in = CNS.createNet2One("in_" + key);
						internNode.addKante(kantekey, in, null);
					} else {
						key = nodeId * 100 + key;
						NetChannelInput in = CNS.createNet2One("out_" + key);
						internNode.addKante(kantekey, in, null);
					}
				}
			}
		}
		
		//Channel Output erstellen dies muss auch in dieser reihenfolge passieren 
		//da es sonst zu einen Deadlock kommt
		for (Graphen.Knoten node : graph.getKnoten()) {
			if (pc == node.getPC().intValue()) {
				int nodeId = node.id.intValue();
				parallelProgramierrung.IKnoten internNode = nodes.get(nodeId);
				for (Graphen.Knoten.Kante kante : node.getKante()) {
					int key = kante.value.intValue();
					int kantekey = key;
					if (key > nodeId) {
						key = key * 100 + nodeId;
						NetChannelOutput out = CNS.createOne2Net("out_" + key);
						Kante kante2 = internNode.getKantenMap().get(kantekey);
						kante2.setChannelOutput(out);
					} else {
						key = nodeId * 100 + key;
						NetChannelOutput out = CNS.createOne2Net("in_" + key);
						internNode.getKantenMap().get(kantekey)
								.setChannelOutput(out);
					}
				}
			}
		}
	}

	/**
	 * @return Den Kanal wo die Ergebnisse ankommen
	 */
	public ChannelInput GetInResultChannel() {
		return inResultChannel;
	}

	/**
	 * Startet die Knoten und damit den Algorytmus
	 * @throws NodeInitFailedException
	 */
	public void Start() {
		setThread(true);
		new Thread(){
			public void run(){
				par.run();
			}
		}.start();
	}

	/**
	 * Stopt samtliche Knoten und Loest die Channels auf
	 */
	public void Stop() {
		Integer i = new Integer(-1);
		setThread(false);
		
		//Sendet an jeden Knoten das Stop Signal
		for (NetChannelOutput control : controllingChannel) {
			control.write(i);
			//Kanele zerstoren damit beim erneuten Start es zu keinen Fehler Kommt.
			try {
				CNS.destroyChannelEnd((NetChannelInput) inResultChannel);
				CNS.destroyChannelEnd(control);
			} catch (Exception ex) {
				log.error("ControllingChannel wurde nicht zerstoert", ex);
			}
		}
	
		//Den Knoten anweisen das Sie intern die Knoten loeschen
		controllingChannel.clear();
		for (IKnoten node : nodes.values()) {
			node.destroyChannels();
		}
	
		//Alle knoten loschen
		nodes.clear();
	}

	/**
	 * Registriert den Lisener die ein Interrese haben an den Fertigen Graph
	 * @param listener Der Lisener
	 */
	public void addListener(IGraphCompletedListener listener) {
		log.debug("Listener hinzugefuegt");
		listeners.add(listener);
	}

	/**
	 * Entfernt den Lisener wieder
	 * @param listener Der zu entferne listener
	 */
	public void removeListener(IGraphCompletedListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Liefert den Thread fuer den Lisener zurueck
	 * @return Der Thread fuer den Lisener
	 */
	public synchronized boolean getThread() {
		return thread;
	}

	/**
	 * Setzt den Therad fuer den Lisener
	 * @param thread Setzt den Thread fuer den Lisener
	 */
	public synchronized void setThread(boolean thread) {
		this.thread = thread;
	}

	/**
	 * Methode die darauf wartet das der Graph Fertig wird um dann das GraphCompletedEvent zu Feuern
	 */
	@Override
	public void run() {
		Integer i = new Integer(1);
		//Giebt allen Knoten das Start zeichen.
		for (ChannelOutput control : controllingChannel) {
			control.write(i);
		}
		
		while (getThread()) {
			if (inResultChannel != null) {
				if (inResultChannel.pending()) {
					Nachricht message = (Nachricht) GetInResultChannel().read();
					log.error("Result erhalten");
					GraphCompletedEvent event = new GraphCompletedEvent(this,
							message.getGraph(), aufgabe);
					for (IGraphCompletedListener l : listeners) {
						try {
							l.graphCompleted(event);
						} catch (Exception ex) {
							log.error("listener hat einen Fehler geworfen", ex);
						}
					}
				} else {
					try {
						Thread.yield();
					} catch (Exception ex) {
						log.debug("Yield abgebrochen", ex);
					}
				}
			}
		}
	}
}
