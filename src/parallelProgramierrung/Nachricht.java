package parallelProgramierrung;

import java.io.Serializable;

/**
 * @author xTNTx
 * Klasse f�r den Nachrichtenaustausch der Knoten untereinander
 */
public class Nachricht implements Serializable{
	/**
	 * UID der Klasse
	 */
	private static final long serialVersionUID = 3077285851430660835L;

	/**ID des Absenders*/
	private final int IDSender;
	
	/**Typ der Nachricht 0=Explorer, 1=Echo*/
	private final MessageType messageType;
	
	/**Die ID die sich durchsetzen soll*/
	private final int MasterID;
	
	/**
	 * Der Auswahl status des Senden Knoten (Optional)
	 */
	private ElectionState electionState;
	
	/**
	 * Der Leader der ermittelt wird oder ermitelt ist (optional)
	 */
	private int Leader;

	/**
	 * Der bekante Graph von dem Knoten (optional)
	 */
	private Graphen graph;
	
	/**
	 * Konstruktor mit den Wichtigsten Parametern
	 * @param IDSender Knoten der die Sendung abgeschickt hat
	 * @param messageType Typ der Nachricht
	 * @param MasterID Die Id die sich durchsetzen soll
	 */
	public Nachricht(int IDSender, MessageType messageType, int  MasterID){
		this.IDSender = IDSender;
		this.MasterID = MasterID;
		this.messageType = messageType;
	}
	
	/**ID des Senders*/
	public int getIDSender(){
		return IDSender;
	}
	
	/**Gibt den Narichten typ zuruek*/
	public MessageType getMessageTyp(){
		return messageType;
	}
	
	/**
	 * @return Die Id die sich durchsetzen soll
	 */
	public int getMasterID(){
		return MasterID;
	}
	
	/**
	 * Gibt den ermittelten Graphen bei einer Echo Nachricht zur�ck
	 * @return Der ermittelte Graph
	 */
	public Graphen getGraph() {
		return graph;
	}

	/**
	 * Setzen des aktuellen ermittelten Graph
	 * @param graph der Aktuelle Teil Graph
	 */
	public void setGraph(Graphen graph) {
		this.graph = graph;
	}
	
	/**
	 * Leifert den ElectionState zurueck
	 * @return Leifert den ElectionState zurueck
	 */
	public ElectionState getElectionState() {
		return electionState;
	}

	/**
	 * Setzt den ElectionState
	 * @param electionState Der Electionstate
	 */
	public void setElectionState(ElectionState electionState) {
		this.electionState = electionState;
	}

	/**
	 * Der Leader
	 * @return Der Leader bedeutung hangt vom Status ab
	 */
	public int getLeader() {
		return Leader;
	}

	/**
	 * Setzt den Leader
	 * @param leader Positiver Integer
	 */
	public void setLeader(int leader) {
		Leader = leader;
	}
	
	/**Enum mit den Nachrichten Typ*/
	public enum MessageType{
		/**
		 * Erkunde Netzwerk
		 */
		Explorer,
		
		/**
		 * Wird gesendet wenn ueber den Kanal keine Informationen zuruek gegeben
		 * wird. (Knoten wurde schon angepingt)
		 */
		NoInfo,
		
		/**
		 * Antwort auf die Explor Nachricht
		 */
		Echo;
	}
}
