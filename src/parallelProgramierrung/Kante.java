package parallelProgramierrung;

import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;

/**
 * @author xTNTx
 * Klasse die eine Ende der Kante im Graph repraesentiert
 */
public class Kante {

	/**
	 * Kanal der ausgeht
	 */
	private org.jcsp.lang.ChannelInput channelInput;
	
	/**
	 * Setzt die Channelinput  Kante
	 * @param channelInput Kanal der ausgeht
	 */
	public synchronized void setChannelInput(org.jcsp.lang.ChannelInput channelInput) {
		this.channelInput = channelInput;
	}

	/**
	 * Setzt den Kanal der Eingeht
	 * @param channelOutput Einghender Kanal
	 */
	public synchronized void setChannelOutput(
			org.jcsp.lang.ChannelOutput channelOutput) {
		this.channelOutput = channelOutput;
	}

	/**
	 * Kanal der eingeht
	 */
	private org.jcsp.lang.ChannelOutput channelOutput;
	
	/**
	 * Ob �ber den Kanal die Erste Nachricht rein kommen ist
	 */
	private boolean SendFirstMessage;
	
	/**
	 * @param channelInput Kanal der ausgeht
	 * @param channelOutput Kanal der eingeht
	 */
	public Kante(org.jcsp.lang.ChannelInput channelInput,
			org.jcsp.lang.ChannelOutput channelOutput) {
		this.channelInput = channelInput;
		this.channelOutput = channelOutput;
	}
	
	/**
	 * @param SendFirstMessage Ob �ber den Kanal die erste Nachricht eingegangen ist
	 */
	public void setSendFirstMessage(boolean SendFirstMessage){
		this.SendFirstMessage = SendFirstMessage;
	}
	
	/**
	 * @return true wenn dies der Kanal ist
	 */
	public boolean getSendFirstMessage(){
		return SendFirstMessage;
	}
	
	/**
	 * @return Den ausgehenden Kanal
	 */
	public ChannelInput getChannelInput() {
		return channelInput;
	}

	/**
	 * @return Den eingehenden Kanal
	 */
	public ChannelOutput getChannelOutput() {
		return channelOutput;
	}
}
