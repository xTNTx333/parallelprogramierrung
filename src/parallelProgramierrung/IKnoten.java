package parallelProgramierrung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.net.NetChannelInput;
import org.jcsp.net.NetChannelOutput;
import org.jcsp.net.cns.CNS;

/**
 * @author xTNTx
 * Abstrakte Klasse die die Knoten darstellt
 */
public abstract class IKnoten implements CSProcess{
	/**ID des Knotens*/
	private int Id;
	
	/**Liste mit allen Kanten zu den Nachbarn */
	private Map<Integer,Kante> kanten = new HashMap<Integer,Kante>();
	
	/**Rueckgabekanal an die GUI*/
	private org.jcsp.lang.ChannelOutput outResultChannel = null;
	
	/**
	 * Logger fuer die Klasse
	 */
	protected static Logger log = Logger.getRootLogger();
	
	/**
	 * Start Knoten der die Processe aufwecken darf
	 */
	private int startNode = 0;
	
	/**
	 * Zeit zwischen den Nachrichten
	 */
	private int messageTime = 1;
	
	/**
	 * Constante fuer die Wartezeit des Timers
	 */
	private int waitForReset = 10000;
	
	/**
	 * Kontoll Kanal fuer die GUI
	 */
	private ChannelInput controllingChannel;
	
	/**
	 * Konstruktor zum erstellen des Knoten
	 * @param ID ID des Knoten
	 * @param outResultChannel Rueckgabe Kanal der Ergebnisse
	 * @param controllingChannel Kontroll Kanal fuer die GUI
	 */
	public IKnoten(int ID, org.jcsp.lang.ChannelOutput outResultChannel, ChannelInput controllingChannel){
		Logger.getLogger(this.getClass());
		this.Id = ID;
		this.setOutResultChannel(outResultChannel);
		this.controllingChannel = controllingChannel;
	}
	
	/**Gibt die eindeutige ID zurueck*/
	public int getIdentifier() {
		return Id;
	}
	
	/**
	 * Gibt den Steuerkanal zuruek
	 * @return der Steuerkanal
	 */
	public ChannelInput getcontrollingChannel(){
		return controllingChannel;
	}

	/**
	 * Fuegt eine Kante dem Knoten hinzu
	 * @param chIn Ausgehender Kanal
	 * @param chOut Eingehender Kanal
	 * @param id ID vom Nachbarknoten
	 */
	public void addKante(Integer id, org.jcsp.lang.ChannelInput chIn, org.jcsp.lang.ChannelOutput chOut) {
			kanten.put(id, new Kante(chIn,chOut));
	}
	
	/**
	 * Fuegt eine Kante dem Knoten hinzu
	 * @param kante Kante vom Graph
	 * @param id vom Nachbarknoten
	 */
	public void addKante(Integer id,Kante kante){
		kanten.put(id, kante);
	}
	
	/**
	 * Reseted den Knoten
	 * @param Id Neue Id des Knoten
	 * @param outResultChannel Neuer Kanal zum uebertragen des Ergebnisses
	 */
	public void ResetKnoten(int Id, org.jcsp.lang.ChannelOutput outResultChannel) {
		kanten.clear();
		this.Id = Id;
		this.setOutResultChannel(outResultChannel);
	}
	
	/**
	 * @return Liefert alle Kanten zurueck
	 */
	public List<Kante> getKanten(){
		List<Kante> temp= new ArrayList<Kante>();
		temp.addAll(kanten.values());
		return temp;
	}
	
	/**
	 * @return Map mit allen Kanten
	 */
	public Map<Integer, Kante> getKantenMap(){
		return kanten;
	}

	/**
	 * Return Channel zum aktualisieren der GUI
	 * @return Kanal zum aktualisieren der GUI
	 */
	public org.jcsp.lang.ChannelOutput getOutResultChannel() {
		return outResultChannel;
	}

	/**
	 * Setzen des Kanals zum aktualisieren der GUI
	 * @param outResultChannel Setzen des Kanals zum aktualisieren der GUI
	 */
	public void setOutResultChannel(org.jcsp.lang.ChannelOutput outResultChannel) {
		this.outResultChannel = outResultChannel;
	}
	
	/**
	 * Entvernt die Kanele aus dem CNS
	 */
	public void destroyChannels(){
		for(Kante kante : kanten.values()){
			try{
				CNS.destroyChannelEnd((NetChannelInput) kante.getChannelInput());
			}catch(Exception ex){
				log.error("Fehler beim Casten: kein Net Channel" , ex);
			}
			
			try{
				CNS.destroyChannelEnd((NetChannelOutput) kante.getChannelOutput());
			}catch(Exception ex){
				log.error("Fehler beim Casten: kein Net Channel" , ex);
			}
			
			try{
				CNS.destroyChannelEnd((NetChannelInput) controllingChannel);
			}catch(Exception ex){
				log.error("Fehler beim Casten: kein Net Channel" , ex);
			}
		}
	}

	/**
	 * Liefert den Start Knoten 
	 * @return Liefert den Start Knoten 
	 */
	public int getStartNode() {
		return startNode;
	}

	/**
	 * Setzt den Start Knoten
	 * @param startNode Positiver Integer 
	 */
	public void setStartNode(int startNode) {
		this.startNode = startNode;
	}

	/**
	 * Liefert die Gesetzte Message Time zuruck
	 * @return Zeit in ms
	 */
	public int getMessageTime() {
		return messageTime;
	}

	/**
	 * Setzt die Message time
	 * @param messageTime Zeit in ms
	 */
	public void setMessageTime(int messageTime) {
		this.messageTime = messageTime;
	}

	/**
	 * Die Zeit bis der Timer anschlagt
	 * @return Zeit in ms
	 */
	public int getWaitForReset() {
		return waitForReset;
	}

	/**
	 * Setzt die Zeit bis der Timer anschlagt
	 * @param waitForReset Zeit in s
	 */
	public void setWaitForReset(int waitForReset) {
		this.waitForReset = waitForReset * 1000;
	}
}
