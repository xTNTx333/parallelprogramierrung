package parallelProgramierrung;

import gui.IPChooser;

import java.awt.Dimension;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JLabel;

import org.jcsp.lang.CSProcess;
import org.jcsp.net.NodeAddressID;
import org.jcsp.net.NodeInitFailedException;
import org.jcsp.net.NodeKey;
import org.jcsp.net.cns.CNS;
import org.jcsp.net.cns.CNSService;
import org.jcsp.net.tcpip.TCPIPAddressID;

/**
 * Klasse stellt den Namen Server fuer die Knoten bereit.
 * @author Felix Bruns
 *
 */
public class CNSServer implements CSProcess {
	/**
	 * Die Adresse worunter der NameServer erreichbar ist
	 */
	private TCPIPAddressID adress;

	/**
	 * Konstruktor zum erstellen des NameServers
	 */
	public CNSServer() {
		try {
			//Vorbereiten das der User aus allen Netzwerk-Schnittstellen die Passende IP
			//raussuchen kann.
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
			List<String> ips = new LinkedList<String>();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> address = networkInterface.getInetAddresses();
                while(address.hasMoreElements()){
                	String addres = address.nextElement().getHostAddress();
                	if(!addres.contains(":")){
                		ips.add(addres);
                	}
                }
            }
            
            IPChooser chooser = new IPChooser();
            chooser.setIpList(ips);
            chooser.setVisible(true);
			adress = new TCPIPAddressID(chooser.getIp(), chooser.getPort(), true);
			
			// Den Server Starten
			NodeKey key = org.jcsp.net.Node.getInstance().init(adress);
			CNS.install(key);
			NodeAddressID cnsAddress = org.jcsp.net.Node.getInstance()
					.getNodeID().getAddresses()[0];
			CNSService.install(key, cnsAddress);
		} catch (IllegalStateException | IllegalArgumentException
				| UnknownHostException e) {
			showError(e);
			
		} catch (NodeInitFailedException e) {
			showError(e);
		} catch (Exception e){
			showError(e);
		}
	}
	
	/**
	 * Methode zum Anzeigen das ein Fehler aufgetreten ist.
	 * @param e Der Fehler der zum aufrufen der Methode fuert
	 */
	private void showError(Throwable e){
		JDialog dialog = new JDialog();
		dialog.setTitle("Error");
		JLabel label = new JLabel();
		label.setText("Adresse konte nicht gefunden werden." +
		"Bitte starten Sie die Anwendung neu. " + e.getLocalizedMessage());
		dialog.add(label);
		dialog.setModal(true);
		dialog.setSize(new Dimension(100, 200));
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Liefert die IP Adresse des Servers zurueck
	 * @return
	 */
	public TCPIPAddressID getAdress() {
		return adress;
	}

	/**
	 * Muss implementiert werden 
	 */
	@Override
	public void run() {
	}

}
