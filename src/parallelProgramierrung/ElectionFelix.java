package parallelProgramierrung;

import java.math.BigInteger;
import java.util.ArrayList;

import org.jcsp.lang.Alternative;
import org.jcsp.lang.AltingChannelInput;
import org.jcsp.lang.CSTimer;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.lang.Guard;
import org.jcsp.lang.One2OneChannel;
import org.jcsp.util.ChannelDataStore;
import org.jcsp.util.InfiniteBuffer;
/**
 * Klasse die den EchoElection Algorithmus implementiert
 * mit dem Zusatz das der Leader selbstst�ndig gew�hlt wird.
 */
public class ElectionFelix extends IKnoten {
	/**
	 * Konstruktor zum erstellen des Knotens
	 * 
	 * @param ID ID des Knotens
	 * @param outResultChannel
	 *            Rueckgabekanal der Ergebnisse
	 * @param controllingChannel Der Kanal der zum Steuern von der GUI aus benutzt wird
	 */
	public ElectionFelix(int ID, ChannelOutput outResultChannel,
			ChannelInput controllingChannel) {
		super(ID, outResultChannel, controllingChannel);
	}

	/** Status in dem sich der Knoten befindet 0=sleep, 1=echo Send 2=close */
	private State state = State.Sleep;

	/**
	 * Liefert den Status des Knoten zurueck
	 * @return Den Status des Knoten
	 */
	public synchronized State getState() {
		return state;
	}

	/**
	 * Setzt den Status des Knotens
	 * @param state Der neue Status des Knotens
	 */
	public synchronized void setState(State state) {
		log.debug("Change State from " + this.state + " to " + state + " ID="
				+ getIdentifier());
		this.state = state;
	}

	/**
	 * Der ermittelte Teilgraph
	 */
	private Graphen graph;

	/**
	 * Die Id des Initiators
	 */
	private int init = Integer.MIN_VALUE;

	/**
	 * Der Kanal wo die Wakeup Nachricht eingegangen ist
	 */
	private int incomingExplorMessage = Integer.MIN_VALUE;

	/**
	 * Zahlt die eingegangenen Echo Nachrichten damit gepr�ft werden kann ob
	 * der Knoten selber ein Echo abschicken muss
	 */
	private int incomingEcho = 0;

	/**
	 * Die gesendeten Message
	 */
	private int sendMessages;

	/**
	 * Timeout zum senden des Nachsten Explore nachricht
	 */
	private long timeout;

	/**
	 * Guard mit alle eingehenden Channels
	 */
	private Guard[] guard;

	/**
	 * Alternative zum ermitteln welcher input channel gewahlt wird
	 */
	private Alternative alt;

	/**
	 * Ausgang der Queue vom Empfanger Thred zum Logik thread
	 */
	private ChannelInput queue;

	/**
	 * Wert dient dazu ob der Knoten an der Leader auswahl teilnehmen mochte
	 */
	private boolean becomeLeader;

	/**
	 * Der aktuelle Leader
	 */
	private int Leader;

	/**
	 * Der Status im den sich der Knoten befindet
	 */
	private ElectionState masterState;
	
	/**
	 * Der Thread der alle Nachrichten annimt
	 */
	private RecieveChannel reciever;

	/**
	 * Methode um den TeilGraphen zu resetten
	 */
	private void resetGraf() {
		graph = new Graphen();
		Graphen.Knoten node = new Graphen.Knoten();
		node.setId(BigInteger.valueOf(this.getIdentifier()));
		graph.getKnoten().add(node);
		sendMessages = 0;
	}

	/**
	 * Durch zufall wird bestimmt ob der Knoten an der Leader auswahl teilnehmen mochte
	 */
	private void randaomeBecomeLeader() {
		becomeLeader = Math.random() > 0.5;
		if (becomeLeader) {
			Leader = this.getIdentifier();
		} else {
			Leader = -1;
		}
	}

	/**
	 * Instanzieiert den Knoten
	 */
	private void init() {
		//Alles auf anfang setzen
		masterState = ElectionState.Election;
		randaomeBecomeLeader();
		resetGraf();
		setState(State.Sleep);
		
		//Alternative erstellen
		ArrayList<Guard> guardChannel = new ArrayList<Guard>();
		for (Kante n : getKantenMap().values()) {
			guardChannel.add(((Guard) n.getChannelInput()));
		}

		CSTimer timer = new org.jcsp.lang.CSTimer();
		timeout = timer.read() + (Math.round(Math.random() * 1000) + 5000);
		timer.setAlarm(timeout);
		guardChannel.add(timer);
		guard = guardChannel.toArray(new Guard[guardChannel.size()]);
		alt = new Alternative(guard);
		
		//Queue erzeugen
		ChannelDataStore buffer = new InfiniteBuffer();
		One2OneChannel channel = org.jcsp.lang.Channel.one2one(buffer);
		queue = channel.in();
		
		//Den Empfanger aller Nachrichten starten
		reciever = new RecieveChannel(alt, guard, channel.out(), this);
		reciever.start();
		ChannelInput controller = getcontrollingChannel();
		
		//Auf das Startsignal warten das es losgehen kann
		controller.read();
	}

	/**
	 * Prueft an wenn die Nachricht weitergereicht werden muss
	 * @param message Die Nachricht die Analysiert werden muss
	 * @param sender Von wem die Nachricht kommt
	 */
	private void proceedMessage(Nachricht message, int sender) {
		if (message.getMessageTyp() == Nachricht.MessageType.Echo) {
			proceedEchoMessage(message);
		} else if (message.getMessageTyp() == Nachricht.MessageType.Explorer) {
			proceedExplorerMessage(message, sender);
		} else if (message.getMessageTyp() == Nachricht.MessageType.NoInfo) {
			proceedNoInfoMessage(message);
		}
	}

	/**
	 * Behandelt die Nachrichten die kein neuen Informations gehalt beinhalten
	 * @param message Nachricht die gesendet worden ist
	 */
	private void proceedNoInfoMessage(Nachricht message) {
		if (init == message.getMasterID()) {
			++incomingEcho;
		}
	}

	/**
	 * Behandelt die Nachrichten von eiem echo
	 * @param message Die Nachricht die Angekommen ist
	 */
	private void proceedEchoMessage(Nachricht message) {
		//Wenn die Nachricht der Runde mit dem Initiator ist, handelt es sich um die 
		//richtig nachricht
		if (message.getMasterID() == init) {
			++incomingEcho;
			//Wenn der Knoten im Status Graph steht enthalt die Nachricht den Teilgraphen
			if (masterState == ElectionState.Graph) {
				//Die Knoten aus den neuen Teilgraph in den aktuellen lokalen Graph einfuegen
				Graphen incomingGraph = message.getGraph();
				for (int i = 0; i < incomingGraph.getKnoten().size(); ++i) {
					parallelProgramierrung.Graphen.Knoten node = incomingGraph
							.getKnoten().get(i);
					//Wenn der Knoten hinzugefuegt wird der die Nachricht gesendet hat setzen wir 
					//die Kante zu diesen Knoten
					if (node.getId().intValue() == message.getIDSender()) {
						parallelProgramierrung.Graphen.Knoten.Kante kante = new Graphen.Knoten.Kante();
						kante.setValue(BigInteger.valueOf(this.getIdentifier()));
						node.getKante().add(kante);
					}
					graph.getKnoten().add(node);
				}

				// Den eigenen Verweis setzen vom eigenden Knoten zum Sender Setzen
				for (parallelProgramierrung.Graphen.Knoten node : graph
						.getKnoten()) {
					if (node.getId() == BigInteger
							.valueOf(this.getIdentifier())) {
						parallelProgramierrung.Graphen.Knoten.Kante kante = new Graphen.Knoten.Kante();
						kante.setValue(BigInteger.valueOf(message.getIDSender()));
						node.getKante().add(kante);
					}
				}
			} else if (masterState == ElectionState.Election) {
				//Wenn der Knoten Leader werden will und der bisherige leader 
				//kleiner ist setzen wir den Leader
				if (becomeLeader) {
					if (Leader < message.getLeader()) {
						Leader = message.getLeader();
					}
				}
			}
		} else {
			log.error("Falsches Echo " + message.toString());
		}
	}

	/**
	 * Behandelt die Explor Nachricht
	 * @param message Nachricht die Empfangen wurde
	 * @param Sender Der Sender der die Nachricht versendet hat
	 */
	private void proceedExplorerMessage(Nachricht message, int Sender) {
		//Wenn wir im State Explore sind
		if (getState() == State.Explorer) {
			//und eine Nachricht bekommen das wir die Abgerufene Information schon
			//versendet haben teilen wir dieses mit
			if (init <= message.getMasterID()) {
				if (getKantenMap().containsKey(message.getIDSender())) {
					Nachricht newMessage = new Nachricht(this.getIdentifier(),
							Nachricht.MessageType.NoInfo, message.getMasterID());

					SendMesage sender = new SendMesage(getKantenMap().get(
							message.getIDSender()).getChannelOutput(),
							newMessage, getIdentifier(), message.getIDSender());
					sender.start();
				}
			} else {
				//Wenn wir den neuen Leader erwarten dann setzen wir den Leader
				//und Starten die Runde 
				//TODO Hier liegt glaube ich der Fehler wir durfen nur neustarten 
				//wenn wir der Leader sind
				if (masterState == ElectionState.Leader
						&& message.getElectionState() == ElectionState.Leader) {
					Leader = message.getLeader();
				}

				initExplorer(message, Sender);
			}
		} else if (getState() == State.Sleep) {
			//Wenn wir geschlafen haben starten wir die Explor Nachricht
			initExplorer(message, Sender);
		}
	}

	/**
	 * Startet den Algorytmus
	 * @param message Nachricht die eingegangen ist
	 * @param Sender Sender der die Nachricht gesendet hat
	 */
	private void initExplorer(Nachricht message, int Sender) {
		resetGraf();
		init = message.getMasterID();
		incomingExplorMessage = message.getIDSender();
		incomingEcho = 0;
		setState(State.Explorer);

		Nachricht newMessage = new Nachricht(this.getIdentifier(),
				Nachricht.MessageType.Explorer, init);
		//Informationen hinzufuegen in welchen Status der Knoten befindet
		if (masterState == ElectionState.Election) {
			newMessage.setElectionState(ElectionState.Election);
		} else if (masterState == ElectionState.Leader) {
			newMessage.setLeader(Leader);
			newMessage.setElectionState(ElectionState.Leader);
		} else if (masterState == ElectionState.Graph) {
			newMessage.setElectionState(ElectionState.Graph);
		}

		for (int i : getKantenMap().keySet()) {
			try {
				//Zeit damit es Interessantere ergebnise kommen
				Thread.sleep(getMessageTime());
			} catch (Exception ex) {
				log.debug("Sleep fuer Message Abgebrochen", ex);
			}
			if (i != message.getIDSender()) {
				SendMesage send = new SendMesage(getKantenMap().get(i)
						.getChannelOutput(), newMessage, this.getIdentifier(),
						i);
				send.start();
				sendMessages = sendMessages + 1;
			}
		}
	}

	/**
	 * Prueft ob ein Echo gesendet werden muss
	 */
	private void checkEchoShouldSend() {
		//Wenn alle gesenden mit den eingehenden Nachrichten uebereinstimmen 
		//und der Knoten auch im passenden Status ist mussen wir pruefen an wenn
		//das Echo gesendet werden soll
		if (sendMessages - incomingEcho == 0 && getState() == State.Explorer) {
			Nachricht newMessage = new Nachricht(this.getIdentifier(),
					Nachricht.MessageType.Echo, init);
			newMessage.setGraph(graph);
			if (init == this.getIdentifier()) {
				//Wenn wir im Status Graph sind dann senden wir die Nachriht an die
				//GUI zurueck
				if (masterState == ElectionState.Graph) {
					log.info("Sende an GUI");
					log.debug(graph.toString());
					this.getOutResultChannel().write(newMessage);
				} else if (masterState == ElectionState.Election) {
					//Wir begeben den neuen Leader bekannt
					Nachricht Election = new Nachricht(this.getIdentifier(),
							Nachricht.MessageType.Explorer, init);
					Election.setLeader(Leader);
					Election.setElectionState(ElectionState.Leader);
					masterState = ElectionState.Graph;
					for (int i : getKantenMap().keySet()) {
						if (i != Election.getIDSender()) {
							SendMesage send = new SendMesage(getKantenMap()
									.get(i).getChannelOutput(), Election,
									this.getIdentifier(), i);
							send.start();
						}
					}
				}
			} else {
				//Senden an den jenigen zuruek der uns aufgewegt hat
				log.debug("Sende Graph an ID: " + incomingExplorMessage);
				SendMesage sender = new SendMesage(getKantenMap().get(
						incomingExplorMessage).getChannelOutput(), newMessage,
						this.getIdentifier(), incomingExplorMessage);
				sender.start();
			}

			setState(State.Sleep);
		}
	}
	
	/**
	 * Die Schleife die den Knoten darstelt
	 */
	@Override
	public void run() {
		init();
		ChannelInput controller = getcontrollingChannel();
		Alternative alt = new Alternative(new Guard[]{(AltingChannelInput) controller, (AltingChannelInput) queue});
		while (true) {
			int select = alt.priSelect();
			if (select == 1) {
				RecievedMessage message = (RecievedMessage) queue.read();
				proceedMessage(message.getMessage(), message.getChannel());
				checkEchoShouldSend();
			}
			if (select == 0) {
				Object message = controller.read();
				if (message instanceof Integer) {
					int i = (Integer) message;
					if (i == -1) {
						reciever.interrupt();
						return;
					}
				}
			}
		}
	}

	/**
	 * Liefert den String des Echos ab
	 */
	@Override
	public String toString() {
		return "Node Nr. " + this.getIdentifier();
	}

	/**
	 * Enum zum pruefen in welchen Status sich der Knoten befindet
	 */
	private enum State {
		Sleep, Explorer, Echo;
	}

	/**
	 * Klasse die zum vermittel der zwischen Empfang und verarbeiten fungiert
	 */
	private class RecievedMessage {
		/**
		 * Nachricht die empfangen wurde
		 */
		private Nachricht message;
		
		/**
		 * Kannalnummer ueber die die Nachricht ankamm
		 */
		private int channel;

		/**
		 * Konstruktor zum Setzen aller Parameter
		 * @param message Nachricht die eingegangen ist
		 * @param channel Kanal in der die Nachricht eingegangen ist
		 */
		public RecievedMessage(Nachricht message, int channel) {
			this.message = message;
			this.channel = channel;
		}

		/**
		 * Giebt die Empfangene Nachricht zurueck
		 * @return Giebt die Empfangene Nachricht zurueck
		 */
		public Nachricht getMessage() {
			return message;
		}

		/**
		 * Liefert den Empfanger Kanal zuruck
		 * @return Liefert den Empfanger Kanal zuruck
		 */
		public int getChannel() {
			return channel;
		}
	}

	/**
	 * Klasse zum Empfangen von Nachrichten anderrer Knoten
	 */
	private class RecieveChannel extends Thread {
		/**
		 * Alternative mit allen Empfengern
		 */
		Alternative alt;
		
		/**
		 * Alle Guards die was empfangen konnen
		 */
		Guard[] guard;
		
		/**
		 * Queue zu der Algorytmus logik
		 */
		ChannelOutput queue;
		
		/**
		 * Ruckverweis auf die Knotenbasis
		 */
		ElectionFelix node;

		/**
		 * Konstrucktor zum setzen aller erforderlichen parameter
		 * @param alt Alternative mit allen Empfengern
		 * @param guard Alle Guards die was empfangen konnen
		 * @param queue Queue zu der Algorytmus logik
		 * @param node Ruckverweis auf die Knotenbasis
		 */
		public RecieveChannel(Alternative alt, Guard[] guard,
				ChannelOutput queue, ElectionFelix node) {
			this.alt = alt;
			this.guard = guard;
			this.queue = queue;
			this.node = node;
		}

		/**
		 * Empfengt alle Nachrichten und sendet sie an die Algorytmus logik
		 */
		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				int select = alt.select();
				//Wenn der Guart der Timmer war
				if (guard[select] instanceof CSTimer) {
					//Timmer neu Setzen
					timeout = timeout
							+ (Math.round(Math.random() * 1000) + getWaitForReset());
					((CSTimer) guard[select]).setAlarm(timeout);
					//Wenn der Knoten noch Schlaefft dann Wecken wir in und Stossen damit 
					//den Algorytmus an
					if (node.getState() == parallelProgramierrung.ElectionFelix.State.Sleep
							&& node.getStartNode() == node.getIdentifier()) {
						Nachricht message = new Nachricht(node.getIdentifier(),
								Nachricht.MessageType.Explorer,
								node.getIdentifier());
						queue.write(new RecievedMessage(message,
								Integer.MIN_VALUE));
					}

				} else if (guard[select] instanceof ChannelInput) {
					//Nachricht auslesen und in den Queue schreiben
					Nachricht message = (Nachricht) ((ChannelInput) guard[select])
							.read();
					RecievedMessage rmessage = new RecievedMessage(message,
							select);
					queue.write(rmessage);
				}
			}
		}
	}

	/**
	 * Klasse zum Senden der Nachricht
	 */
	private class SendMesage {
		/**
		 * Channel an wen die Nachricht gehen soll
		 */
		private final ChannelOutput outputChannel;
		
		/**
		 * Die Nachricht die gesendet werden soll
		 */
		private final Nachricht message;

		/**
		 * Konstrucktor der Alle Parameter setzt
		 * @param outputChanel Channel an wen die Nachricht gehen soll
		 * @param message Die Nachricht die gesendet werden soll
		 * @param ID Die ID des Knoten
		 * @param nach Die Id des zu senden Konoten
		 */
		public SendMesage(ChannelOutput outputChanel, Nachricht message,
				int ID, int nach) {
			log.debug(ID + " nach " + nach + " Type=" + message.getMessageTyp()
					+ " MasterID=" + message.getMasterID());
			this.message = message;
			this.outputChannel = outputChanel;
		}

		/**
		 * Sendet die Nachticht los
		 */
		public void start() {
			outputChannel.write(message);
		}
	}
}
