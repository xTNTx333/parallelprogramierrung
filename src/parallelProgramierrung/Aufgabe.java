package parallelProgramierrung;
/**
 * Enum zum unterscheiden welche Aufgabe durchgefuehrt werden soll
 * @author Felix Bruns
 *
 */
public enum Aufgabe {
	/**
	 * Startet den Echo Algorithmus zu Aufgabe 1
	 */
	echoAufgabe1,
	
	/**
	 * Startet den Election Algorithmus zu Aufgabe 1
	 */
	electionAufgabe1,
	
	/**
	 * Startet den Algorithmus zu Aufgabe 2
	 */
	electionAufgabe3
}
