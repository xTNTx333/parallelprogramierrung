package parallelProgramierrung;

import java.math.BigInteger;
import java.util.ArrayList;

import org.jcsp.lang.Alternative;
import org.jcsp.lang.AltingChannelInput;
import org.jcsp.lang.CSTimer;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.lang.Guard;
import org.jcsp.lang.One2OneChannel;
import org.jcsp.util.ChannelDataStore;
import org.jcsp.util.InfiniteBuffer;
/**
 * Klasse die den EchoElection Algorithmus implementiert
 */
public class Echo2 extends IKnoten {
	/**
	 * Konstruktor zum erstellen des Knotens
	 * 
	 * @param ID
	 *            ID des Knotens
	 * @param outResultChannel
	 *            Rueckgabekanal der Ergebnisse
	 * @param controllingChannel Der Kanal der zum Steuern von der GUI aus benutzt wird
	 */
	public Echo2(int ID, ChannelOutput outResultChannel, ChannelInput controllingChannel) {
		super(ID, outResultChannel, controllingChannel);
	}

	/** Status in dem sich der Knoten befindet 0=sleep, 1=echo Send 2=close */
	private State state = State.Sleep;

	/**
	 * Liefert den Status des Knoten zurueck
	 * @return Den Status des Knoten
	 */
	public synchronized State getState() {
		return state;
	}

	/**
	 * Setzt den Status des Knotens
	 * @param state Der neue Status des Knotens
	 */
	public synchronized void setState(State state) {
		log.debug("Change State from " + this.state + " to " + state + " ID=" + getIdentifier());
		this.state = state;
	}

	/**
	 * Der ermittelte Teilgraph
	 */
	private Graphen graph;

	/**
	 * Die Id des Initiators
	 */
	private int init = Integer.MIN_VALUE;

	/**
	 * Der Kanal wo die Wakeup Nachricht eingegangen ist
	 */
	private int incomingExplorMessage = Integer.MIN_VALUE;

	/**
	 * Zahlt die eingegangenen Echo Nachrichten damit gepr�ft werden kann ob
	 * der Knoten selber ein Echo abschicken muss
	 */
	private int incomingEcho = 0;

	/**
	 * Die anzahl der gesedeten Nachrichten
	 */
	private int sendMessages;

	/**
	 * Zeit fuer das timeout des Knotens
	 */
	private long timeout;

	/**
	 * Alle Channels die in die eine Nachricht empfangen koennen
	 */
	private Guard[] guard;

	/**
	 * Alternative damit nich unmengen an Zeit verbraten wird
	 */
	private Alternative alt;

	/**
	 * Der Queue input zum Analysieren der Nachrichten
	 */
	private ChannelInput queue;
	
	/**
	 * Der Thread der alle Nachrichten annimt
	 */
	private RecieveChannel reciever;
	
	/**
	 * Methode um den TeilGraphen zu resetten
	 */
	private void resetGraf() {
		graph = new Graphen();
		Graphen.Knoten node = new Graphen.Knoten();
		node.setId(BigInteger.valueOf(this.getIdentifier()));
		graph.getKnoten().add(node);
		sendMessages = 0;
	}

	/**
	 * Instanzieiert den Knoten
	 */
	private void init() {
		//Alles auf anfang setzen
		resetGraf();
		setState(State.Sleep);
		
		//Alternative erstellen
		ArrayList<Guard> guardChannel = new ArrayList<Guard>();
		for (Kante n : getKantenMap().values()) {
			guardChannel.add(((Guard) n.getChannelInput()));
		}

		CSTimer timer = new org.jcsp.lang.CSTimer();
		timeout = timer.read() + (Math.round(Math.random() * 1000) + 5000);
		timer.setAlarm(timeout);
		guardChannel.add(timer);
		guard = guardChannel.toArray(new Guard[guardChannel.size()]);
		alt = new Alternative(guard);
		
		//Queue erzeugen
		ChannelDataStore buffer = new InfiniteBuffer();
		One2OneChannel channel = org.jcsp.lang.Channel.one2one(buffer);
		queue = channel.in();
		
		//Den Empfanger aller Nachrichten starten
		reciever = new RecieveChannel(alt, guard, channel.out(), this);
		reciever.start();
		ChannelInput controller = getcontrollingChannel();
		
		//Auf das Startsignal warten das es losgehen kann
		controller.read();
	}

	/**
	 * Prueft an wenn die Nachricht weitergereicht werden muss
	 * @param message Die Nachricht die Analysiert werden muss
	 * @param sender Von wem die Nachricht kommt
	 */
	private void proceedMessage(Nachricht message, int sender) {
		if (message.getMessageTyp() == Nachricht.MessageType.Echo) {
			proceedEchoMessage(message);
		} else if (message.getMessageTyp() == Nachricht.MessageType.Explorer) {
			proceedExplorerMessage(message, sender);
		} else if (message.getMessageTyp() == Nachricht.MessageType.NoInfo) {
			proceedNoInfoMessage(message);
		}
	}

	/**
	 * Behandelt die Nachrichten die kein neuen Informations gehalt beinhalten
	 * @param message Nachricht die gesendet worden ist
	 */
	private void proceedNoInfoMessage(Nachricht message) {
		if (init == message.getMasterID()) {
			++incomingEcho;
		}
	}

	/**
	 * Behandelt die Nachrichten von eiem echo
	 * @param message Die Nachricht die Angekommen ist
	 */
	private void proceedEchoMessage(Nachricht message) {
		//Es mussen nur Echo Nachrichten berucksichtig werden die sich vom durchgeseztem Knoten
		//kommen
		if (message.getMasterID() == init) {
			++incomingEcho;
			Graphen incomingGraph = message.getGraph();
			//Die Knoten aus den neuen Teilgraph in den aktuellen lokalen Graph einfuegen
			for (int i = 0; i < incomingGraph.getKnoten().size(); ++i) {
				parallelProgramierrung.Graphen.Knoten node = incomingGraph.getKnoten().get(i);
				//Wenn der Knoten hinzugefuegt wird der die Nachricht gesendet hat setzen wir 
				//die Kante zu diesen Knoten
				if (node.getId().intValue() == message.getIDSender()) {
					parallelProgramierrung.Graphen.Knoten.Kante kante = new Graphen.Knoten.Kante();
					kante.setValue(BigInteger.valueOf(this.getIdentifier()));
					node.getKante().add(kante);
				}
				graph.getKnoten().add(node);
			}

			// Den eigenen Verweis setzen vom eigenden Knoten zum Sender Setzen
			for (parallelProgramierrung.Graphen.Knoten node : graph.getKnoten()) {
				if (node.getId() == BigInteger.valueOf(this.getIdentifier())) {
					parallelProgramierrung.Graphen.Knoten.Kante kante = new Graphen.Knoten.Kante();
					kante.setValue(BigInteger.valueOf(message.getIDSender()));
					node.getKante().add(kante);
				}
			}
		} else {
			log.debug("Falsches Echo " + message.toString());
		}
	}

	/**
	 * Behandelt die Explor Nachricht
	 * @param message Nachricht die Empfangen wurde
	 * @param Sender Der Sender der die Nachricht versendet hat
	 */
	private void proceedExplorerMessage(Nachricht message, int Sender) {
		if (getState() == State.Explorer) {
			//Wenn die Nachricht einen kleinern oder Gleichen wert von init hat
			//Giebt es keine neuen Informations gehalt da die Information schon 
			//Abgerufen wurde
			if (init <= message.getMasterID()) {
				Nachricht newMessage = new Nachricht(this.getIdentifier(), Nachricht.MessageType.NoInfo,
						message.getMasterID());
				SendMesage sender = new SendMesage(getKantenMap().get(message.getIDSender()).getChannelOutput(),
						newMessage, getIdentifier(), message.getIDSender());
				sender.start();
			} else {
				//Wir Starten neu weil eine Starkere welle diesen Knoten ueberrollt
				initExplorer(message, Sender);
			}
		} else if (getState() == State.Sleep) {
			initExplorer(message, Sender);
		}
	}

	/**
	 * Startet den Algorytmus
	 * @param message Nachricht die eingegangen ist
	 * @param Sender Sender der die Nachricht gesendet hat
	 */
	private void initExplorer(Nachricht message, int Sender) {
		resetGraf();
		init = message.getMasterID();
		incomingExplorMessage = message.getIDSender();
		incomingEcho = 0;
		setState(State.Explorer);

		Nachricht newMessage = new Nachricht(this.getIdentifier(), Nachricht.MessageType.Explorer, init);
		for (int i : getKantenMap().keySet()) {
			//Zeit damit es Interessantere ergebnise kommen
			try{
				Thread.sleep(getMessageTime());
			}catch(Exception ex){
				log.debug("Sleep fuer Message Abgebrochen", ex);
			}
			
			//Der eigende Sender muss die Nachricht nicht an sich selber senden
			if (i != message.getIDSender()) {
				SendMesage send = new SendMesage(getKantenMap().get(i).getChannelOutput(), newMessage,
						this.getIdentifier(), i);
				send.start();
				sendMessages = sendMessages + 1;
			}
		}
	}

	/**
	 * Prueft ob ein Echo gesendet werden muss
	 */
	private void checkEchoShouldSend() {
		//Wenn alle gesenden mit den eingehenden Nachrichten uebereinstimmen 
		//und der Knoten auch im passenden Status ist mussen wir pruefen an wenn
		//das Echo gesendet werden soll
		if (sendMessages - incomingEcho == 0 && getState() == State.Explorer) {
			Nachricht newMessage = new Nachricht(this.getIdentifier(), Nachricht.MessageType.Echo, init);
			newMessage.setGraph(graph);
			//Wenn wir der Iniziator des Algorytmus waren senden wir das Ergebnis an die GUI
			if (init == this.getIdentifier()) {
				log.info("Sende an GUI");
				log.debug(graph.toString());
				this.getOutResultChannel().write(newMessage);
			} else {
				//Senden an den jenigen zuruek der uns aufgewegt hat
				log.debug("Sende Graph an ID: " + incomingExplorMessage);
				SendMesage sender = new SendMesage(getKantenMap().get(incomingExplorMessage).getChannelOutput(),
						newMessage, this.getIdentifier(), incomingExplorMessage);
				sender.start();
			}

			setState(State.Sleep);
		}
	}

	/**
	 * Die Schleife die den Knoten darstelt
	 */
	@Override
	public void run() {
		init();
		ChannelInput controller = getcontrollingChannel();
		Alternative alt = new Alternative(new Guard[]{(AltingChannelInput) controller, (AltingChannelInput) queue});
		while (true) {
			//Wir setzen die Prieoritat hoer der Steuernachrichten als der Algo Nachrichten
			int select = alt.priSelect();
			if (select == 1) {
				RecievedMessage message = (RecievedMessage) queue.read();
				proceedMessage(message.getMessage(), message.getChannel());
				checkEchoShouldSend();
			}
			if (select == 0) {
				Object message = controller.read();
				if (message instanceof Integer) {
					int i = (Integer) message;
					if (i == -1) {
						reciever.interrupt();
						return;
					}
				}
			}
		}
	}

	/**
	 * Liefert den String des Echos ab
	 */
	@Override
	public String toString() {
		return "Node Nr. " + this.getIdentifier();
	}

	/**
	 * Enum zum pruefen in welchen Status sich der Knoten befindet
	 */
	private enum State {
		Sleep, Explorer, Echo;
	}

	/**
	 * Klasse die zum vermittel der zwischen Empfang und verarbeiten fungiert
	 */
	private class RecievedMessage {
		/**
		 * Nachricht die empfangen wurde
		 */
		private Nachricht message;
		
		/**
		 * Kannalnummer ueber die die Nachricht ankamm
		 */
		private int channel;

		/**
		 * Konstruktor zum Setzen aller Parameter
		 * @param message Nachricht die eingegangen ist
		 * @param channel Kanal in der die Nachricht eingegangen ist
		 */
		public RecievedMessage(Nachricht message, int channel) {
			this.message = message;
			this.channel = channel;
		}

		/**
		 * Giebt die Empfangene Nachricht zurueck
		 * @return Giebt die Empfangene Nachricht zurueck
		 */
		public Nachricht getMessage() {
			return message;
		}

		/**
		 * Liefert den Empfanger Kanal zuruck
		 * @return Liefert den Empfanger Kanal zuruck
		 */
		public int getChannel() {
			return channel;
		}
	}

	/**
	 * Klasse zum Empfangen von Nachrichten anderrer Knoten
	 */
	private class RecieveChannel extends Thread {
		/**
		 * Alternative mit allen Empfengern
		 */
		Alternative alt;
		
		/**
		 * Alle Guards die was empfangen konnen
		 */
		Guard[] guard;
		
		/**
		 * Queue zu der Algorytmus logik
		 */
		ChannelOutput queue;
		
		/**
		 * Ruckverweis auf die Knotenbasis
		 */
		Echo2 node;

		/**
		 * Konstrucktor zum setzen aller erforderlichen parameter
		 * @param alt Alternative mit allen Empfengern
		 * @param guard Alle Guards die was empfangen konnen
		 * @param queue Queue zu der Algorytmus logik
		 * @param node Ruckverweis auf die Knotenbasis
		 */
		public RecieveChannel(Alternative alt, Guard[] guard, ChannelOutput queue, Echo2 node) {
			this.alt = alt;
			this.guard = guard;
			this.queue = queue;
			this.node = node;
		}

		/**
		 * Empfengt alle Nachrichten und sendet sie an die Algorytmus logik
		 */
		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				int select = alt.select();
				//Wenn der Guart der Timmer war
				if (guard[select] instanceof CSTimer) {
					//Timmer neu Setzen
					timeout = timeout + (Math.round(Math.random() * 1000) + getWaitForReset());
					((CSTimer) guard[select]).setAlarm(timeout);
					//Wenn der Knoten noch Schlaefft dann Wecken wir in und Stossen damit 
					//den Algorytmus an
					if (node.getState() == parallelProgramierrung.Echo2.State.Sleep
							&& node.getStartNode() == node.getIdentifier()) {
						Nachricht message = new Nachricht(node.getIdentifier(), Nachricht.MessageType.Explorer,
								node.getIdentifier());
						queue.write(new RecievedMessage(message, Integer.MIN_VALUE));
					}

				} else if (guard[select] instanceof ChannelInput) {
					//Nachricht auslesen und in den Queue schreiben
					Nachricht message = (Nachricht) ((ChannelInput) guard[select]).read();
					RecievedMessage rmessage = new RecievedMessage(message, select);
					queue.write(rmessage);
				} 
			}
		}
	}

	/**
	 * Klasse zum Senden der Nachricht
	 */
	private class SendMesage {
		/**
		 * Channel an wen die Nachricht gehen soll
		 */
		private final ChannelOutput outputChannel;
		
		/**
		 * Die Nachricht die gesendet werden soll
		 */
		private final Nachricht message;

		/**
		 * Konstrucktor der Alle Parameter setzt
		 * @param outputChanel Channel an wen die Nachricht gehen soll
		 * @param message Die Nachricht die gesendet werden soll
		 * @param ID Die ID des Knoten
		 * @param nach Die Id des zu senden Konoten
		 */
		public SendMesage(ChannelOutput outputChanel, Nachricht message, int ID, int nach) {
			log.debug(ID + " nach " + nach + " Type=" + message.getMessageTyp() + " MasterID=" + message.getMasterID());
			this.message = message;
			this.outputChannel = outputChanel;
		}

		/**
		 * Sendet die Nachticht los
		 */
		public void start() {
			outputChannel.write(message);
		}
	}
}
