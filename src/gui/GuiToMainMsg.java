package gui;

import java.io.Serializable;
/**
 * Nachrichten Klasse zum Ein- und Ausloggen von Client Nodes
 */
public class GuiToMainMsg implements Serializable {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pc Nummer die Gewahlt wurde
	 */
	private int m_Pc;
	
	/**
	 * Art der Nachricht
	 */
	private MessageType m_MsgType;
	
	/**
	 * Konstruktor mit den wichtigsten Parametern
	 * @param pc Gewaehlte PC Nummer
	 * @param msgType Ob es sich um eine Login oder Logout Nachricht handelt
	 */
	public GuiToMainMsg(int pc, MessageType msgType){
		m_Pc = pc;
		m_MsgType = msgType;
	}
	
	/**
	 * Gibt den Typ der Nachricht zurueck
	 * @return Login/Logout
	 */
	public MessageType getMessageType() {
		return m_MsgType;
	}
	
	/**
	 * Der Pc identifier
	 * @return Positiver Integer
	 */
	public int getPc() {
		return m_Pc;
	}
	
	/**Enum fuer den Nachrichten Typ*/
	public enum MessageType{
		Login,
		Logout
	}
}
