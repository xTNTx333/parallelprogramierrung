package gui;

import java.util.EventListener;

/**
 * Interface fuer alle die Interesse an den ermittelten Graphen haben
 */
public interface IGraphCompletedListener extends EventListener {
	/**
	 * Wird aufgerufen, wenn der Ergbnisgraph ermittelt wurde
	 * @param e Informationen rund um den Graph
	 */
	public void graphCompleted(GraphCompletedEvent e);
	
}
