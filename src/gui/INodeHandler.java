package gui;

import java.util.Collection;

import parallelProgramierrung.Graphen;
/**
 * Interface mit Methoden zum Steuern von GUI Elementen 
 */
public interface INodeHandler {

	/**
	 * Fuegt eine Kante hinzu
	 * @param node1 ID des Ersten Knoten
	 * @param node2 ID des Zweiten Knoten
	 */
	public void addEdge(int node1, int node2);
	
	/**
	 * Entfernt eine Kante
	 * @param node1 ID des Ersten Knoten
	 * @param node2 ID des Zweiten Knoten
	 */
	public void removeEdge(int node1, int node2);
	
	/**
	 * Erstelt einen Knoten
	 * @return Der erstellte Knoten
	 */
	public IVisualNode createNode();
	
	/**
	 * Loescht den Knoten aus dem Graph
	 * @param id ID des zum Loeschenden Knoten
	 */
	public void deleteNode(int id);
	
	/**
	 * Setzt den PC des Knoten
	 * @param nodeID ID des Knoten
	 * @param pc neue Pc Nummer
	 */
	public void setPc(int nodeID, int pc);
	
	/**
	 * Liefert den Kompletten Graphen zurueck
	 * @return Der Graph der Angezeigt wird
	 */
	public Graphen getGraph();
	
	/**
	 * Liefert alle Knoten IDs zurueck
	 * @return Collection mit allen Node IDs
	 */
	public Collection<Integer> getAllNodeIDs();
}
