package gui;

import java.util.EventObject;

import parallelProgramierrung.Aufgabe;
import parallelProgramierrung.Graphen;

/**
 * Event das die Gui den ermittelten Graphen uebergiebt
 *
 */
public class GraphCompletedEvent extends EventObject{
	
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Der Graph der Ermittelt wurde
	 */
	private Graphen graph;
	
	/**
	 * Die Algorytmus der Angewand wurde
	 */
	private Aufgabe aufgabe;
	
	/**
	 * Konstructor des Events
	 * @param source Auslosser des Events
	 * @param graph Der Ermittelte Graph
	 * @param aufgabe Der Algorytmus
	 */
	public GraphCompletedEvent(Object source, Graphen graph, Aufgabe aufgabe) {
		super(source);
		this.graph = graph;
		this.aufgabe = aufgabe;
	}

	/**
	 * Gibt den Graphen zuruck
	 * @return Den Graphen
	 */
	public Graphen getGraph() {
		return graph;
	}

	/**
	 * Liefert den Algorytmus zuruck
	 * @return Liefert den Algorytmus zuruck
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

}
