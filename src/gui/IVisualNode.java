package gui;

import java.awt.BorderLayout;

public abstract class IVisualNode extends javax.swing.JPanel{

	/**
	 * Abstrakte Klasse die Visuell einen Knoten darstellt
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * ID des Knoten
	 */
	protected int id;
	
	/**
	 * Node Handler der Informiert werden muss
	 */
	protected INodeHandler handler;
	
	/**
	 * Konstruktor fuer VisualNode
	 * @param id Id des Knoten
	 * @param handler Handler der Informiert werden muss
	 * @param editable Ob die Anzeige veraendert werden darf
	 */
	public IVisualNode(int id, INodeHandler handler, boolean editable) {
		super(new BorderLayout());
		this.id=id;
		this.handler = handler;
	}
	
	/**
	 * Fuegt eine Kante hinzu
	 * @param node Id des fremden Knotens
	 */
	public abstract void addEdge(int node);
	
	/**
	 * Entfernt die Kante wieder
	 * @param node ID des fremden Knoten
	 */
	public abstract void removeEdge(int node);
	
	/**
	 * Setzt den PC
	 * @param value PC Nummer
	 */
	public abstract void setPc(int value);
	
	/**
	 * Setzt ob der Knoten Editierbar ist oder nicht
	 * @param setTo true = Editierbar
	 */
	public abstract void setActive(boolean setTo);
	
	/**
	 * Loescht alle Verweise
	 */
	public void dispose(){
		handler = null;
	}
}