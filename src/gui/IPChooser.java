package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.jcsp.net.tcpip.TCPIPCNSServer;
/**
 * Dialog zum auswaehlen von Ip und Port
 */
public class IPChooser extends JDialog{

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pannel zum anzeigen
	 */
	private JPanel contentPane;
	
	/**
	 * Textfeld zum eingeben des Ports
	 */
	private JFormattedTextField portField = new JFormattedTextField();
	
	/**
	 * Listmodel mit den IPs
	 */
	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	
	/**
	 * Die gewaehlte IP
	 */
	private String ip;
	
	/**
	 * Der gewaehlte Port
	 */
	private int port;
	
	/**
	 * Giebt die gewaehlte IP zuruck
	 * @return Die gewaehlte IP
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Der Gewaehlte Port
	 * @return der Port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Konstruktor der die Oberflache vorbereitet
	 */
	public IPChooser(){
		// Allgemeine Einstellungen fuer das Fenster
		setTitle("IP fuer den Server waehlen");
		setBounds(200, 100, 130, 100);
		setMinimumSize(new Dimension(130, 100));
		setModal(true);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		// Liste mit den Kanten zu diesem Knoten
		final JList<String> ipList = new JList<String>(listModel);
		ipList.setLayoutOrientation(JList.VERTICAL);
		ipList.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.white));
		ipList.setVisibleRowCount(3);
		JScrollPane listScroller = new JScrollPane(ipList);
		listScroller.setPreferredSize(new Dimension(100, 80));
		contentPane.add(listScroller, BorderLayout.CENTER);
		
		// SOUTH - Buttonleiste
		JPanel southPanel = new JPanel();
		southPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		southPanel.setLayout(new BorderLayout(0, 0));
		contentPane.add(southPanel, BorderLayout.SOUTH);
		
		JPanel btnPanel = new JPanel();
		btnPanel.setBorder(null);
		btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		southPanel.add(btnPanel, BorderLayout.SOUTH);
		
		portField.setValue(new Integer(TCPIPCNSServer.DEFAULT_CNS_PORT));
		southPanel.add(portField, BorderLayout.CENTER);
		
		JButton okButton = new JButton("OK");
		btnPanel.add(okButton);
		
		okButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				ip = ipList.getSelectedValue();
				port = (int) portField.getValue();
				dispose();
			}
			
		});
		
		pack();
	}

	/**
	 * Setzt die auswaehlbaren IPs
	 * @param ipList Liste mit IPs
	 */
	public void setIpList(List<String> ipList) {
		for (int i = 0; i < ipList.size(); ++i){
			listModel.addElement(ipList.get(i));
		}
	}

}
