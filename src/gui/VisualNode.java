package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.log4j.Logger;
/**
 * Stellt einen Knoten aus dem Graph grafisch dar
 */
public class VisualNode extends IVisualNode {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Liste mit allen Kanten
	 */
	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	
	/**
	 * Spinner zur auswahl auf welchen PC der Knoten laufen soll
	 */
	private JSpinner pcSpinner;
	
	/**
	 * Menueeintrag um den Knoten wieder zu entfernen
	 */
	JMenuItem removeNode = new JMenuItem("Knoten entfernen");
	
	/**
	 * Aendert die Beschriftung vom Butten
	 */
	private boolean flagAdd = false;
	
	/**
	 * Auswahlliste  zum Auswaehlen zu welchen Knoten eine Kante gezogen werden soll
	 */
	private final JComboBox<Integer> idChooser = new JComboBox<Integer>();
	
	/**
	 * Button zum hinzufuegen oder entfernen von einer Kante
	 */
	final JButton addOrRemove = new JButton("Kante auswaehlen");
	
	/**
	 * Logger zum loggen von ereignissen
	 */
	private static Logger log = Logger.getLogger(VisualNode.class);

	/**
	 * Konstruktor zum erstelen der GUI
	 * @param id ID des Knoten
	 * @param handler der Nodehandler, der den Knoten erstellt hat
	 * @param showButton Ob die Gui editierbar sein soll
	 */
	public VisualNode(final int id, final INodeHandler handler, boolean showButton) {
		super(id, handler, showButton);
		setLayout(new BorderLayout());
		setBorder(new LineBorder(Color.DARK_GRAY,2,true));
		setSize(200, 400);
		
		// Norden - Allgemeine Properties des Knotens
		JPanel northPanel = new JPanel(new GridLayout(2,2,1,5));
		northPanel.setBorder(new EtchedBorder());
		
		SpinnerNumberModel spModel = new SpinnerNumberModel(0,0,99,1);
		pcSpinner = new JSpinner(spModel);
		
		northPanel.add(new JLabel("ID:"));
		northPanel.add(new JLabel("" + id));
		northPanel.add(new JLabel("PC:"));
		northPanel.add(pcSpinner);
		add(northPanel, BorderLayout.NORTH);
		
		// Zentrum - Nachbarn des Knotens
		JPanel centerPanel = new JPanel(new FlowLayout());
		centerPanel.setBorder(new TitledBorder(null, "Kanten", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		// Liste mit den Kanten zu diesem Knoten
		JList<String> edgeList = new JList<String>(listModel);
		edgeList.setLayoutOrientation(JList.VERTICAL);
		edgeList.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.white));
		edgeList.setVisibleRowCount(5);
		JScrollPane listScroller = new JScrollPane(edgeList);
		listScroller.setPreferredSize(new Dimension(175, 100));
		
		centerPanel.add(listScroller);
		add(centerPanel, BorderLayout.CENTER);
		
		if (showButton) {
			// Sueden - Wird nur angezeigt, wenn der Knoten editierbar ist
			JPanel southPanel = new JPanel(new FlowLayout());
			southPanel.setBorder(new EtchedBorder());
	
			southPanel.add(idChooser);
			southPanel.add(addOrRemove);
			add(southPanel, BorderLayout.SOUTH);
			
			// Aktuell laufende Knoten vom Handler anfordern und beim ausklappen der Combobox anzeigen
			idChooser.addPopupMenuListener(new PopupMenuListener() {
				public void popupMenuCanceled(PopupMenuEvent arg0) {}
				public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
					addOrRemove.grabFocus();
					log.debug(idChooser.getSelectedItem());
				}

				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {
					DefaultComboBoxModel<Integer> comboModel = new DefaultComboBoxModel<Integer>();	
					Collection<Integer> nodeCollection = handler.getAllNodeIDs();
					for (int node : nodeCollection ) {
						if (id != node){
							comboModel.addElement(node);
						}
					}
					idChooser.setModel(comboModel);
				}
				
			});
			
			// Nach der Auswahl eines Knotens aus der Kombobox den Auswahlbutton anpassen
			idChooser.addFocusListener(new FocusListener() {

				@Override
				public void focusGained(FocusEvent arg0) {}

				@Override
				public void focusLost(FocusEvent arg0) {
					log.debug("focus lost at: " + idChooser.getSelectedItem());
					if (idChooser.getSelectedItem() != null){
						if(listModel.contains(idChooser.getSelectedItem().toString())){
							log.debug("contains number: " + idChooser.getSelectedItem());
							nextIsAdd(false);
						}
						else {	
							nextIsAdd(true);
						}
					}
				}
				
			});
			
			idChooser.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent arg0) {
					idChooser.transferFocus();
				}
				
			});
			
			// Kante hinzufuegen, wenn nicht vorhanden, sonst entfernen
			addOrRemove.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					addOrRemove.grabFocus();
					log.debug("button pressed!");					
					if(idChooser.getSelectedItem() != null) {
						if(flagAdd) {
							handler.addEdge(id, (int) idChooser.getSelectedItem());
							nextIsAdd(false);
						}
						else {
							handler.removeEdge(id, (int) idChooser.getSelectedItem());
							nextIsAdd(true);
						}
					}
				}
			});
			
			// Popupmenue zum loeschen des Knotens
			final JPopupMenu menu = new JPopupMenu();
			setComponentPopupMenu(menu);
			menu.add(removeNode);
			addMouseListener(new MouseListener() {
				public void mouseReleased(MouseEvent e) {}
				public void mousePressed(MouseEvent e) {}
				public void mouseExited(MouseEvent e) {}
				public void mouseEntered(MouseEvent e) {}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					if(SwingUtilities.isRightMouseButton(e)){
						menu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			});
			
			// Handler mitteilen, dass dieser Knoten geloescht werden soll
			removeNode.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e){
					handler.deleteNode(id);
					dispose();
				}
			});
		}
		
		else {
			pcSpinner.setEnabled(false);	
		}
		
		// Handler informieren, wenn neuer Pc zugeordnet wird
		pcSpinner.addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	            handler.setPc(id, getPc());
	        }
	    });
	}
	
	/**
	 * Fuegt eine Kante zum uebergebenen Knoten ein
	 * @param node ID des Fremden Knoten
	 */
	@Override
	public void addEdge(int node) {
		listModel.addElement("" + node);
		if (idChooser.getSelectedItem() != null && (int) idChooser.getSelectedItem() == node) {
			nextIsAdd(false);
		}
	}
	
	/**
	 * Loescht die Kante zum uebergebenen Knoten
	 * @param node ID des Fremden Knoten
	 */
	@Override
	public void removeEdge(int node) {
		listModel.removeElement("" + node);
		if (idChooser.getSelectedItem() != null && (int) idChooser.getSelectedItem() == node) {
			nextIsAdd(true);
		}
	}

	/**
	* Methode zum setzen des PC Spinners
	* @param value PC Nummer
	*/
	public void setPc(int value){
		if (0 < value && value < 100){
			pcSpinner.setValue(value);
		}	
	}
	
	/**
	* Gibt den zur Zeit eingestellten Wert des PC auswahlkaestchens zurueck
	*/
	public int getPc(){
		return (int) pcSpinner.getValue();
	}
	
	/**
	 *  Aendert die Beschriftung und Funktion des Buttons
	 */
	private void nextIsAdd(boolean b){
		if (b) {
			addOrRemove.setText("Kante hinzufuegen");
			flagAdd = true;
		}
		else {
			addOrRemove.setText("Kante entfernen");
			flagAdd = false;
		}
	}
	
	/**
	 *  Aktiviert oder deaktiviert die Komponenten im Knotenfenter
	 */
	public void setActive(boolean setTo){
		addOrRemove.setEnabled(setTo);
		idChooser.setEnabled(setTo);
		pcSpinner.setEnabled(setTo);
		removeNode.setEnabled(setTo);
	}
}
