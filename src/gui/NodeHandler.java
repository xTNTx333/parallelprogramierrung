package gui;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import parallelProgramierrung.Graphen;

/**
 * Klasse zum abhandeln der Knoteneigenschaften
 * @author xTNTx
 *
 */
public class NodeHandler implements INodeHandler {
	public NodeHandler(JComponent pannelToDraw, boolean editable){
		this.pannelToDraw = pannelToDraw;
		this.editable = editable;
	}
	
	/**
	 * Gibt an ob sich das Control editieren lassen soll
	 */
	private boolean editable;
	
	/**
	 * Komponente auf der gezeichnet werden soll
	 */
	private JComponent pannelToDraw;
	
	/**
	 * Map mit den visuell dargestelten Knoten
	 */
	private Map<Integer, IVisualNode> visualNodes = new HashMap<Integer, IVisualNode>();
	
	/**
	 * Knoten die im Algorithmus verwendet werden
	 */
	private Map<Integer, Graphen.Knoten> graphNodes = new HashMap<Integer, Graphen.Knoten>();
	
	/**
	 * Der Class logger
	 */
	private static Logger log = Logger.getLogger(NodeHandler.class);
	
	/**
	 * Prueft ob ein Knoten schon vorhanden ist
	 * @param nodes Knoten
	 * @return True wenn der Knoten nicht vorhanden ist
	 */
	private boolean containsNodeIDs(Integer... nodes){
		for(Integer i : nodes){
			if(!(visualNodes.keySet().contains(i) && graphNodes.keySet().contains(i))){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Hilfsmethode zum hinzufuegen der Kanten
	 * @param node1 Knoten 1
	 * @param node2 Knoten 2
	 */
	private void addInnerEdge(int node1, int node2){
		IVisualNode vNode = visualNodes.get(node1);
		Graphen.Knoten gNode = graphNodes.get(node1);
		vNode.addEdge(node2);
		Graphen.Knoten.Kante kante = new Graphen.Knoten.Kante();
		kante.setValue(BigInteger.valueOf(node2));
		gNode.getKante().add(kante);
	}
	
	/**
	 * Methode zum hinzufuegen der Kanten
	 * @param node1 Knoten 1
	 * @param node2 Knoten 2
	 */
	@Override
	public void addEdge(int node1, int node2) {
		log.debug("Add Edge aufgerufen");
		if(containsNodeIDs(node1, node2)){
			addInnerEdge(node1, node2);
			addInnerEdge(node2, node1);
		}else{
			throw new RuntimeException("Nicht alle Knoten sind vorhanden");
		}
	}
	
	/**
	 * Hilfsmethode zum entfernen der Kante
	 * @param node1 Knoten 1
	 * @param node2 Knoten 2
	 */
	private void removeInnerEdge(int node1, int node2){
		IVisualNode vNode = visualNodes.get(node1);
		vNode.removeEdge(node2);		
		removeEdgeFromGraph(node1, node2);
	}

	/**
	 * HilfsMethode zum entfernen der Kante vom Graphen
	 * @param node1 Knoten 1
	 * @param node2 Knoten 2
	 */
	private void removeEdgeFromGraph(int node1, int node2) {
		Graphen.Knoten gNode = graphNodes.get(node1);
		Graphen.Knoten.Kante rKante = null;
		for(Graphen.Knoten.Kante kante: gNode.getKante()){
			if(kante.getValue() == BigInteger.valueOf(node2)){
				rKante = kante;
			}
		}
		
		gNode.getKante().remove(rKante);
	}
	
	/**
	 * Methode zum entfernen der Kante
	 * @param node1 Knoten 1
	 * @param node2 Knoten 2
	 */
	@Override
	public void removeEdge(int node1, int node2) {
		if(containsNodeIDs(node1, node2)){
			removeInnerEdge(node1, node2);
			removeInnerEdge(node2, node1);
		}else{
			throw new RuntimeException("Nicht alle Knoten sind vorhanden");
		}
	}

	/**
	 * Erzeugt einen neuen Knoten
	 */
	@Override
	public IVisualNode createNode() {
		int id = 0;
		boolean foundResult = false;
		while(!foundResult){
			if(!visualNodes.keySet().contains(id)){
				foundResult = true;
			}else{
				id = id + 1;
			}
		}
		
		repaint();
		return createInnerNode(id);
	}

	/**
	 * Hilfsmethode zum Erstellen eines Knotens
	 * @param id id des zu erzeugenden Knotens
	 * @return der Erzeugte Knoten
	 */
	private IVisualNode createInnerNode(int id) {
		VisualNode vNode = new VisualNode(id, this, editable);
		Graphen.Knoten gNode = new Graphen.Knoten();
		gNode.setId(BigInteger.valueOf(id));
		gNode.setPC(BigInteger.valueOf(0));
		visualNodes.put(id, vNode);
		graphNodes.put(id, gNode);
		pannelToDraw.add(vNode);
		return vNode;
	}

	/**
	 * Hilfsmethode zum entfernen des Knotens
	 * @param id Id des Knotens
	 */
	@Override
	public void deleteNode(int id) {
		if(containsNodeIDs(id)){
			IVisualNode vNode = visualNodes.get(id);
			Graphen.Knoten gNode = graphNodes.get(id);
			for(Graphen.Knoten.Kante kante : gNode.getKante()){
				Integer nodeId = kante.getValue().intValue();
				removeEdgeFromGraph(nodeId, id);
				IVisualNode rvNode = visualNodes.get(nodeId);
				rvNode.removeEdge(id);
			}
			
			visualNodes.remove(id);
			graphNodes.remove(gNode);
			pannelToDraw.remove(vNode);
			vNode.dispose();
			repaint();
		}else{
			throw new RuntimeException("Knoten nicht vorhanden");
		}		
	}

	/**
	 * Setzt den PC
	 * @param nodeID ID des Knotens
	 * @param pc PC des Knotens
	 */
	@Override
	public void setPc(int nodeID, int pc){
		Graphen.Knoten node = graphNodes.get(nodeID);
		node.setPC(BigInteger.valueOf(pc));
	}

	/**
	 * Liefert alle Knoten ID's
	 */
	@Override
	public Collection<Integer> getAllNodeIDs() {
		return graphNodes.keySet();
	}
	
	/**
	 * Lade den Graphen ein
	 * @param graph Graph der eingeladen werden soll
	 */
	public void LoadGraphen(Graphen graph){
		removeAllNodes();
		try{
		for(Graphen.Knoten knoten : graph.getKnoten()){
			int pc = knoten.getPC() != null ? knoten.getPC().intValue() : 0;
			IVisualNode node = this.createInnerNode(knoten.getId().intValue());
			node.setPc(pc);
			pannelToDraw.revalidate();
		}
		for(Graphen.Knoten knoten : graph.getKnoten()){
			for(Graphen.Knoten.Kante kante: knoten.getKante()){
				this.addInnerEdge(knoten.getId().intValue(), kante.getValue().intValue());
			}
		}
		}catch(Exception ex){
			JOptionPane.showMessageDialog(pannelToDraw, 
					"Ein Fehler ist aufgetreten! +" + ex.getMessage(),
					"Fehler", JOptionPane.OK_OPTION);
		}
		
		repaint();
	}
	
	/**
	 * Entfernt alle Knoten
	 */
	private void removeAllNodes(){
		for(IVisualNode node : visualNodes.values()){
			pannelToDraw.remove(node);
			node.dispose();
		}
		
		visualNodes.clear();
		graphNodes.clear();
	}
	
	public Graphen getGraph(){
		Graphen graph = new Graphen();
		graph.getKnoten().addAll(graphNodes.values());
		return graph;
	}
	
	/**
	 * Zeichnet den Content neu
	 */
	private void repaint(){
		pannelToDraw.revalidate();
		pannelToDraw.repaint();
	}
	
	/**
	 * Setzt die GUI Knoten Aktiv oder Deaktiv
	 * @param setTo True gleich aktiv
	 */
	public void setActive(boolean setTo){
		for (IVisualNode node : visualNodes.values()){
			node.setActive(setTo);
		}
		
		repaint();
	}
}
