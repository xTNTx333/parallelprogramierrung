package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.net.LinkLostException;
import org.jcsp.net.NetAltingChannelInput;
import org.jcsp.net.NetSharedChannelOutput;
import org.jcsp.net.NodeInitFailedException;
import org.jcsp.net.cns.CNS;
import org.jcsp.net.tcpip.TCPIPAddressID;
import org.jcsp.net.tcpip.TCPIPCNSServer;
import org.jcsp.net.tcpip.TCPIPNodeFactory;

import parallelProgramierrung.GeneratedGraph;
/*
 * Klasse fuer die Client GUI
 */
public class NodeGUI extends JFrame implements WindowListener {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Die Haupt Pain
	 */
	private JPanel contentPane;
	
	/**
	 * Flag ob der Client eingelogt ist
	 */
	private boolean loggedInFlag = false;
	
	/**
	 * Nummer mit dem der Client eingelogt ist
	 */
	private int pcNumber;
	
	/**
	 * Buttons zum Registrieren und Benden
	 */
	private JButton btnRegisterNode, btnBeenden;
	
	/**
	 * Label wo der Status angezeigt wird
	 */
	private JLabel lblStatus = new JLabel("");
	
	/**
	 * Channel zur Main GUI zum ein und aus Loggen
	 */
	NetSharedChannelOutput channel;
	
	/**
	 * Channel zum Starten der Algorytmen
	 */
	NetAltingChannelInput controller;
	
	/**
	 * Logger zum loggen der Nachrichten
	 */
	private static Logger log = Logger.getLogger(NodeGUI.class);

	/**
	 * Baut die GUI auf
	 */
	public NodeGUI() {

		// Allgemeine Einstellungen fuer das Fenster
		setTitle("Echo-/Electionalgorithmus");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(200, 100, 330, 200);
		setMinimumSize(new Dimension(330, 200));
		addWindowListener(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		// SOUTH - Buttons
		JPanel btnPanel = new JPanel();
		contentPane.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		btnRegisterNode = new JButton("Knoten anmelden");
		btnPanel.add(btnRegisterNode);

		JLabel lblButtonSeparator = new JLabel("      ");
		btnPanel.add(lblButtonSeparator);

		btnBeenden = new JButton("Beenden");
		btnPanel.add(btnBeenden);

		// NORTH - Labels
		JPanel mainPanel = new JPanel();
		contentPane.add(mainPanel, BorderLayout.NORTH);
		mainPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel dataPanel = new JPanel();
		dataPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Einstellungen",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		mainPanel.add(dataPanel);
		dataPanel.setLayout(new GridLayout(2, 2, 0, 0));

		JLabel lblIDText = new JLabel("Knoten-ID");
		dataPanel.add(lblIDText);

		SpinnerNumberModel spModel = new SpinnerNumberModel(1, 1, 99, 1);
		final JSpinner spID = new JSpinner(spModel);
		dataPanel.add(spID);

		JLabel lblIPText = new JLabel("IP Adresse (Server)");
		dataPanel.add(lblIPText);

		final JTextField txtIP = new JTextField();
		try {
			txtIP.setText(new TCPIPAddressID(InetAddress.getLocalHost()
					.getHostAddress(), TCPIPCNSServer.DEFAULT_CNS_PORT, true).toString());
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		dataPanel.add(txtIP);

		contentPane.add(lblStatus, BorderLayout.CENTER);
		
		// Eventhandler
		/**
		 * Meldet den Knoten beim Hauptknoten an
		 */
		btnRegisterNode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!loggedInFlag) {
					login(txtIP.getText(), (int) spID.getValue());
					if (loggedInFlag) {
						btnRegisterNode.setText("Knoten abmelden");
					}
				} else {
					try{
						logout();
					} catch (Exception ex) {
						log.error("Server war nicht erreichbar" , ex);
						cleanUp();
					}
					if (!loggedInFlag) {
						btnRegisterNode.setText("Knoten anmelden");
					}
				}
			}
		});
		/**
		 * Loest den Knoten aus dem Netzwerk und beendet die Anwendung
		 */
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					logout();
				} catch (Exception ex) {
					log.error("Server war nicht erreichbar" , ex);
					cleanUp();
				}
				System.exit(0);
			}
		});
	}

	 
	/**
	 * Meldet den Knoten beim Server ab und gibt zurueck, ob der Knoten jetzt
	 * abgemeldet ist
	 * @return True wenn logout erfolgt ist
	 */
	private boolean logout() {
		if (loggedInFlag) {
			try{
				ChannelOutput out = channel;
				out.write(new GuiToMainMsg(pcNumber, GuiToMainMsg.MessageType.Logout));
				CNS.destroyChannelEnd(controller);
				controller = null;
				loggedInFlag = false;
				return true;			}
			catch (LinkLostException ex){
				log.error("Verbindung zum Server verloren!", ex);
				cleanUp();
			}
		}
		return true;
	}

	/**
	 * wird aufgerufen, um das Fenster aufzuraeumen, wenn die Verbindung zum server verloren wird
	 */	
	private void cleanUp() {
		controller = null;
		loggedInFlag = false;	
		btnBeenden.setEnabled(true);
		btnRegisterNode.setEnabled(true);
		lblStatus.setText("Waiting for next Start.");
	}
	
	/**
	 * Meldet den Knoten beim Server an und gibt zurueck, ob der Knoten jetzt
	 * angemeldet ist
	 * @param ip Die IP zum CNS
	 * @param pc Die PC Nummer die Vergeben werden soll
	 * @return True wenn login erfolgreich ist
	 */
	private boolean login(String ip, int pc) {
		if (!loggedInFlag) {
			pcNumber = pc;

			try {
				if (channel == null) {
					org.jcsp.net.Node.getInstance().init(new TCPIPNodeFactory(ip));
					channel = CNS.createAny2Net("GUIMessageChannel");
				}

				ChannelOutput out = channel;
				out.write(new GuiToMainMsg(pcNumber, GuiToMainMsg.MessageType.Login));
				controller = CNS.createNet2One("Client" + pc);
			} catch (NodeInitFailedException e) {
				e.printStackTrace();
				return false;
			}

			loggedInFlag = true;

			new Thread() {
				private Object lock = new Object();
				GeneratedGraph genGraph = null;

				public GeneratedGraph getGenGraph() {
					return genGraph;
				}

				public void setGenGraph(GeneratedGraph genGraph) {
					this.genGraph = genGraph;
				}

				@SuppressWarnings("deprecation")
				public void run() {
					//Solange der Controller eingeloggt ist warten wir auf Nachrichten
					while (loggedInFlag) {
						if (controller != null && controller.pending()) {
							Object temp = controller.read();
							if (temp instanceof MainToNode) {
								final MainToNode message = (MainToNode) temp;
								//Wenn es sich um eine Start Nachricht handelt starten wir 
								//den uebergebenden Graphen in der Nachticht. Sonst Stoppen 
								//wir den Laufenden Graphen
								if (message.isStart()) {
									synchronized (lock) {
										if (message.getGraph() != null) {
											new Thread() {
												public void run() {
													setGenGraph(new GeneratedGraph(message.getGraph(), pcNumber,
															message.getAufgabe(), message.getStartNode(), message.getMessageTime(), message.getWaitForReset()));
													btnBeenden.setEnabled(false);
													btnRegisterNode.setEnabled(false);
													lblStatus.setText("Running...");
													try {
														getGenGraph().Start();
													} catch (Exception e) {
														log.error("Fehler beim starten des Graphen",e);
													}
												}
											}.start();
										}
									}
								} else {
									synchronized (lock) {
										if (getGenGraph() != null) {
											getGenGraph().Stop();
											setGenGraph(null);
											btnBeenden.setEnabled(true);
											btnRegisterNode.setEnabled(true);
											lblStatus.setText("Waiting for next Start.");
										}
									}
								}
							}
						}
					}
				}
			}.start();
		}
		return true;
	}

	/**
	 * Behandelt das Schliessen des Fensters
	 */
	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
	}
}
