package gui;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
/**
 * GUI zum entscheiden was gestartet werden soll (Server oder normaler Knoten)
 */
public class StartGUI extends JFrame {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Hauptpannel
	 */
	private JPanel contentPane;
	
	/**
	 * Logger zum loggen von Nachrichten
	 */
	private static Logger log = Logger.getRootLogger();

	/**
	 * Oeffnet das Auswahlfenster
	 */
	public static void main(String[] args) {		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Layout layout = new org.apache.log4j.xml.XMLLayout();
					FileAppender appender = new FileAppender(layout, "log.log", false);
					log.addAppender(appender);
					Logger.getRootLogger().setLevel(Level.DEBUG);
					StartGUI frame = new StartGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Baut das Auswahlfenster
	 */
	public StartGUI() {
		// Allgemeine GUI-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 200, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(2, 0, 0, 0));
		setContentPane(contentPane);
		
		// GUI Elemente
		JLabel lbl = new JLabel("Waehle den Knotentyp aus:");
		contentPane.add(lbl);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// Button fuer einen normalen Knoten
		JButton btnKnoten = new JButton("Knoten");
		panel.add(btnKnoten);
		
		// Button fuer einen Serverknoten
		JButton btnServer = new JButton("Server");
		panel.add(btnServer);
		
		// Komprimieren
		pack();
		
		// Eventhandler
		btnKnoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NodeGUI frame = new NodeGUI();
				frame.setVisible(true);
				dispose();
			}
		});
		btnServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainGUI frame = new MainGUI();
				frame.setVisible(true);
				dispose();
			}
		});
	}
}
