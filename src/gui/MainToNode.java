package gui;

import java.io.Serializable;

import parallelProgramierrung.Aufgabe;
import parallelProgramierrung.Graphen;
/**
 * Nachricht zum Stoppen oder Starten vom Algorithmus auf den Clients
 */
public class MainToNode implements Serializable{
	
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1561305841538110567L;
	
	/**
	 * Was fuer ein Algorithmus gestartet werden soll
	 */
	private Aufgabe aufgabe;
	
	/**
	 * Der Graph der gestartet werden soll
	 */
	private Graphen graph;
	
	/**
	 * Ob es sich um eine Start oder Stop Nachricht handelt
	 */
	private boolean start;
	
	/**
	 * Start Knoten
	 */
	private Integer startNode;
	
	/**
	 * Wartezeit zwischen Nachrichten
	 */
	private Integer messageTime;
	
	/**
	 * Wartezeit fuer den Timmer
	 */
	private Integer waitForReset;
	
	/**
	 * Konstruktor zum fuellen aller wichtigen Properties
	 * @param start Ob es sich um eine Start Nachricht handelt
	 * @param aufgabe Der Algorithmus der gestartet werden soll
	 * @param graph der Graph der gestartet werden soll
	 * @param startNode der Start Knoten
	 * @param messageTime Zeit zwischen den Nachrichten
	 * @param waitForReset Wartezeit fuer den Timmer
	 */
	public MainToNode(boolean start, Aufgabe aufgabe, Graphen graph, Integer startNode, Integer messageTime, Integer waitForReset){
		this.start = start;
		this.graph = graph;
		this.aufgabe = aufgabe;
		this.startNode = startNode;
		this.messageTime = messageTime;
		this.waitForReset = waitForReset;
	}

	/**
	 * Gibt den zu startenden Algorithmus zurueck
	 * @return Der zu startende Algorithmus
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * Der Graph der gestartet werden soll
	 * @return Der Graph
	 */
	public Graphen getGraph() {
		return graph;
	}

	/**
	 * Gibt an, ob es sich um eine Start Nachricht handelt
	 * @return true wenn es eine Start Nachricht ist
	 */
	public boolean isStart() {
		return start;
	}

	/**
	 * Gibt den Start Knoten zurueck
	 * @return StartKnoten ID
	 */
	public Integer getStartNode() {
		return startNode;
	}
	
	/**
	 * Gibt die Wartezeit zwischen zwei Nachrichten zurueck
	 * @return Wartezeit ms
	 */
	public Integer getMessageTime(){
		return messageTime;
	}

	/**
	 * Wartezeit fuer den Timmer 
	 * @return Wartezeit in s
	 */
	public Integer getWaitForReset() {
		return waitForReset;
	}
}
