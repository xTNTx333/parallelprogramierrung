package gui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
/**
 * Klasse die einen Dialog zum auswaehlen des Start Knotens anzeigt 
 */
public class ChooseStartNode {
	/**
	 * Das Ergebnis welcher Startknoten ausgewahlt worden ist
	 */
	private int result = 0;
	
	/**
	 * Baut den Dialog auf und zeigt ihn an
	 * @param collection Alle auswaehlbaren Knoten
	 * @param owner Vater Fenster
	 */
	public ChooseStartNode(Collection<Integer> collection, Frame owner){
		if(collection != null && !collection.isEmpty()){
			List<Integer> list = new LinkedList<>(collection);
			Collections.sort(list);
			result = list.get(0);
			
			final JDialog dialog = new JDialog(owner, "Waehle Startknoten", true);
			dialog.setLayout(new BorderLayout());
			JPanel panel = new JPanel();
			panel.add(new JLabel("Start Knoten"));
			final JSpinner spinner = new JSpinner(new SpinnerListModel(list));
			panel.add(spinner);
			
			dialog.add(panel, BorderLayout.CENTER);
			
			JButton OK = new JButton("OK");
			OK.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					result = (Integer)spinner.getValue();
					dialog.setVisible(false);
					dialog.dispose();
				}
			});
			
			dialog.add(OK, BorderLayout.SOUTH);
			dialog.pack();
			dialog.setVisible(true);
		}
	}

	/**
	 * Liefert den Startknoten, der vom Benutzer gewahlt worden bzw. zufaellig gewaehlt worden ist
	 * @return Das Ergenis
	 */
	public int getResult() {
		return result;
	}
}
