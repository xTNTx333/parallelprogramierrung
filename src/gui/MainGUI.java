package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jcsp.lang.Alternative;
import org.jcsp.lang.Guard;
import org.jcsp.net.NetAltingChannelInput;
import org.jcsp.net.NetChannelOutput;
import org.jcsp.net.cns.CNS;

import parallelProgramierrung.Aufgabe;
import parallelProgramierrung.CNSServer;
import parallelProgramierrung.GeneratedGraph;
import parallelProgramierrung.Graphen;
import parallelProgramierrung.Helper;
/**
 * Das Fenster stellt alle Einstellungsmoeglichkeiten fuer den Graph bereit
 * und laesst ihn auch starten
 */
public class MainGUI extends JFrame implements IGraphCompletedListener {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Hauptpannel
	 */
	private JPanel contentPane = new JPanel();
	
	/**
	 * CNS Server zum ermitteln von Net Kanaelen
	 */
	private CNSServer cnsServer;
	
	/**
	 * Node Handler der den Editeirbaren Graphen enthaelt
	 */
	private NodeHandler editableNodeHandler = null;
	
	/**
	 * Kanal zum einloggen von Client Fenstern
	 */
	private NetAltingChannelInput m_inputClient;
	
	/**
	 * Steuerkanal von allen Clients
	 */
	private Map<Integer, NetChannelOutput> m_clients = new HashMap<Integer, NetChannelOutput>();
	
	/**
	 * Liste mit allen abgemeldeten Clients
	 */
	private final JList<Integer> list = new JList<Integer>();
	
	/**
	 * Die Klasse die den spanneden Baum ermittelt
	 */
	private GeneratedGraph graph;
	
	/**
	 * Fuegt einen Knoten hinzu
	 */
	private JMenuItem addNode = new JMenuItem("Knoten hinzuf\u00FCgen");
	
	/**
	 * Der NodeHandler der das Ergebnis behandelt
	 */
	private NodeHandler finishNodeHandler = null;
	
	/**
	 * Logger zum loggen
	 */
	private Logger log = Logger.getLogger("MainGUI");
	
	/**
	 * Slider fuer die Zeit zwischen zwei Nachrichten
	 */
	private JSlider timeSlider = new JSlider();
	
	/**
	 * Slider zum einstellen der Timer fuer den Neustart der Knoten
	 */
	private JSlider restWaitTimeSlider = new JSlider();
	
	/**
	 * Button zum Starten des Echo-Algorithmus
	 */
	private JButton btnEcho = new JButton("Echo-Algorithmus starten");
	
	/**
	 * Button zum Starten des Election-Algorithmus
	 */
	private JButton btnElection = new JButton("Election-Algorithmus starten");
	
	/**
	 * Button zum Starten der Echo-Election Aufgabe
	 */
	private JButton btnRandom = new JButton("Zufaellige Election starten");
	
	/**
	 * Stoppt den Algorithmus
	 */
	private JButton btnStop = new JButton("Stoppen");
	
	/**
	 * Schliesst das Fenster
	 */
	private JButton btnExit = new JButton("Schlie\u00DFen");
	
	/**
	 * Importiert den Graphen
	 */
	private JButton importButton = new JButton("Import XML");
	
	/**
	 * Exportiert den Graphen
	 */
	private JButton exportButton = new JButton("Export XML");
	
	/**
	 * Main Methode zum Anzeigen des Fensters(Testzwecke)
	 * @param args Werden nicht verwendet
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI frame = new MainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Baut das GUI auf
	 */
	public MainGUI() {

		// Fenster
		setTitle("Echo-/Electionalgorithmus - Administration");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		setMinimumSize(new Dimension(900, 450));

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		// SOUTH - Buttonleiste
		JPanel btnPanel = new JPanel();
		btnPanel.setBorder(null);
		contentPane.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		btnPanel.add(btnEcho);
		btnPanel.add(btnElection);
		btnPanel.add(btnRandom);
		btnPanel.add(btnStop);
		btnPanel.add(new JLabel("               "));
		btnPanel.add(btnExit);

		// CENTER - Graph
		final JPanel center = new JPanel();
		center.setBackground(Color.WHITE);
		FlowLayout centerGrid = new FlowLayout();
		centerGrid.setAlignment(FlowLayout.LEFT);
		center.setLayout(centerGrid);
		editableNodeHandler = new NodeHandler(center, true);
		JScrollPane centerScrolePane = new JScrollPane(center);
		centerScrolePane.setMaximumSize(center.getPreferredSize());
		centerScrolePane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		centerScrolePane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		centerScrolePane.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Graph konfigurieren",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));

		// EAST - Der aufgespannte Graph des letzten Algorithmus
		JPanel east = new JPanel();
		FlowLayout estFlow = new FlowLayout();
		estFlow.setAlignment(FlowLayout.LEFT);
		east.setLayout(estFlow);
		finishNodeHandler = new NodeHandler(east, false);
		JScrollPane eastScrollPane = new JScrollPane(east);
		eastScrollPane.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Ergebnisgraph",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		JSplitPane centerSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				centerScrolePane, eastScrollPane);
		centerSplit.setResizeWeight(0.5);
		contentPane.add(centerSplit, BorderLayout.CENTER);

		// NORTHWEST - Knotenliste
		JPanel leftPanel = new JPanel();
		leftPanel.setBorder(null);
		contentPane.add(leftPanel, BorderLayout.WEST);
		leftPanel.setLayout(new GridLayout(0, 1));

		JPanel topleftPanel = new JPanel();
		topleftPanel.setMinimumSize(new Dimension(100, 400));
		topleftPanel.setLayout(new BorderLayout(0, 1));
		topleftPanel.setBorder(new TitledBorder(null, "Angemeldete PCs",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		leftPanel.add(topleftPanel);

		DefaultListModel<Integer> dlist = new DefaultListModel<Integer>();
		dlist.addElement(0);
		list.setModel(dlist);
		list.setSelectedIndex(0);
		list.setDragEnabled(false);

		// Popupmenue fuer die Knotenliste
		final JPopupMenu menu = new JPopupMenu();
		list.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int index = list.locationToIndex(e.getPoint());
					list.setSelectedIndex(index);
					menu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		// fuegt neuen Knoten ein
		menu.add(addNode);
		center.setComponentPopupMenu(menu);
		addNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editableNodeHandler.createNode();
			}
		});

		// ScrollPane fuer die Knotenliste
		JScrollPane listPane = new JScrollPane(list);
		listPane.setViewportView(list);
		topleftPanel.add(listPane);

		// WEST - Import
		JPanel midleftPanel = new JPanel();
		midleftPanel.setLayout(new GridLayout(0, 1));
		JPanel midleftTopPanel = new JPanel();
		midleftTopPanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Min. Wartezeit nach Durchlauf in s",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		restWaitTimeSlider.setMinimum(10);
		restWaitTimeSlider.setMaximum(60);
		restWaitTimeSlider.setValue(10);
		restWaitTimeSlider.setMajorTickSpacing(10);
		restWaitTimeSlider.setMinorTickSpacing(5);
		restWaitTimeSlider.setPaintLabels(true);
		restWaitTimeSlider.setPaintTicks(true);
		midleftTopPanel.add(restWaitTimeSlider);
		midleftPanel.add(midleftTopPanel);
		
		JPanel midleftMidlePanel = new JPanel();
		midleftMidlePanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Zeit zwischen Nachrichten in ms",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		timeSlider.setMaximum(2000);
		timeSlider.setMajorTickSpacing(500);
		timeSlider.setMinorTickSpacing(100);
		timeSlider.setMinimum(0);
		timeSlider.setValue(10);
		timeSlider.setPaintTicks(true);
		timeSlider.setPaintLabels(true);
		midleftMidlePanel.add(timeSlider);
		midleftPanel.add(midleftMidlePanel);
		
		JPanel midleftBottonPanel = new JPanel();
		midleftBottonPanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Import/Export",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		midleftBottonPanel.setBackground(new Color(240, 240, 240));
		midleftBottonPanel.setLayout(new GridLayout(1, 1, 5, 2));
		midleftPanel.add(midleftBottonPanel);
		leftPanel.add(midleftPanel, BorderLayout.SOUTH);
		
		JButton exportButton = new JButton("Export XML");
		midleftBottonPanel.add(exportButton);
		
		exportButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Graphen graph = editableNodeHandler.getGraph();
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(false);
				chooser.setCurrentDirectory(new File(System
						.getProperty("user.home")));
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"XML - Dateien", "xml");
				chooser.addChoosableFileFilter(filter);
				chooser.setFileFilter(filter);
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setApproveButtonText("Speichern");
				
				if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					File file = chooser.getSelectedFile();
					if(!file.getName().endsWith(".xml")){
						file = new File(file.getAbsolutePath() + ".xml");
					}
					
					Helper.SaveGraphen(graph, file);
				}
			}
		});
		
		
		midleftBottonPanel.add(importButton);
		// Eventhandler
		btnEcho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startAlgo(Aufgabe.echoAufgabe1);
			}
		});

		btnElection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startAlgo(Aufgabe.electionAufgabe1);
			}
		});

		btnRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startAlgo(Aufgabe.electionAufgabe3);
			}
		});

		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stopGraph();
			}
		});

		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		importButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// JFileChooser-Objekt konfigurieren
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(false);
				chooser.setCurrentDirectory(new File(System
						.getProperty("user.home")));
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"XML - Dateien", "xml");
				chooser.addChoosableFileFilter(filter);
				chooser.setFileFilter(filter);
				chooser.setAcceptAllFileFilterUsed(false);

				// Dialog zum Oeffnen der XML-Datei anzeigen
				int rueckgabeWert = chooser.showOpenDialog(null);

				if (rueckgabeWert == JFileChooser.APPROVE_OPTION) {
					try {
						Graphen graph = Helper.getGraphen(new File(chooser
								.getSelectedFile().getPath()));
						editableNodeHandler.LoadGraphen(graph);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		// CNS Server starten
		cnsServer = new CNSServer();
		setTitle(getTitle() + " - " + cnsServer.getAdress());
		new Thread() {
			@SuppressWarnings("deprecation")
			public void run() {
				m_inputClient = CNS.createNet2One("GUIMessageChannel");
				Alternative alt = new Alternative(new Guard[]{m_inputClient});
				while (true) {
					alt.select();
					if (m_inputClient.pending()) {
						log.debug("pending");
						DefaultListModel<Integer> listModel = (DefaultListModel<Integer>) list
								.getModel();
						Object message = m_inputClient.read();
						if (message instanceof GuiToMainMsg) {
							GuiToMainMsg msg = (GuiToMainMsg) message;
							int pc = msg.getPc();
							if (msg.getMessageType() == GuiToMainMsg.MessageType.Login
									&& !listModel.contains(pc)) {
								listModel.addElement(pc);
								NetChannelOutput out = CNS
										.createOne2Net("Client" + pc);
								m_clients.put(pc, out);
							} else if (msg.getMessageType() == GuiToMainMsg.MessageType.Logout
									&& listModel.contains(pc)) {
								int posision = listModel.indexOf(pc);
								listModel.remove(posision);
								NetChannelOutput out = m_clients.get(pc);
								CNS.destroyChannelEnd(out);
								m_clients.remove(pc);
							}
						} else {
							try {
								Thread.sleep(200);
								log.debug("Schlafen beendet");
							} catch (Exception ex) {

							}
						}
					}
				}
			}
		}.start();

		this.setVisible(true);
	}

	/**
	 * Startet den uebergebenen Algorithmus
	 */ 
	private void startAlgo(final Aufgabe aufgabe) {

		final Graphen graph1 = editableNodeHandler.getGraph();
		if (graph1.getKnoten().isEmpty()) {
			final JDialog dialog = new JDialog(this, "Fehler", true);
			BorderLayout layout = new BorderLayout();
			dialog.setLayout(layout);
			JTextArea message = new JTextArea(
					"Der Graph muss mindestens ein Knoten Enthalten");
			message.setEditable(false);
			dialog.add(message, BorderLayout.CENTER);
			dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			JButton OK = new JButton("OK");
			OK.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					dialog.setVisible(false);
					dialog.dispose();
				}
			});
			dialog.add(OK, BorderLayout.SOUTH);
			dialog.setSize(200, 100);
			dialog.pack();
			dialog.setVisible(true);
		} else {
			enabeltForm(false);
			ChooseStartNode chooser = null;
			int temp = 0;
			if (aufgabe == Aufgabe.echoAufgabe1) {
				chooser = new ChooseStartNode(
						editableNodeHandler.getAllNodeIDs(), this);
				temp = chooser.getResult();
			}
			final Integer startNode = temp;
			final MainGUI frame = this;
			new Thread() {
				public void run() {
					try {
						MainToNode mesage = new MainToNode(true, aufgabe,
								graph1, startNode, timeSlider.getValue(), restWaitTimeSlider.getValue());
						for (NetChannelOutput out : m_clients.values()) {
							out.write(mesage);
						}
						graph = new GeneratedGraph(
								editableNodeHandler.getGraph(), 0, aufgabe, startNode, timeSlider.getValue(), restWaitTimeSlider.getValue());
						graph.addListener(frame);
						graph.Start();
					} catch (Exception e) {
						log.error("Fehler beim Starten des Graphen", e);
					}
				}
			}.start();
		}
	}

	/**
	* Wird ausgefuehrt, wenn ein Graph fertigestellt wurde
	*/
	@Override
	public void graphCompleted(GraphCompletedEvent e) {
		System.out.println("ErgebnisGUI wurde informiert");
		finishNodeHandler.LoadGraphen(e.getGraph());
		if(e.getAufgabe() != Aufgabe.electionAufgabe3){
			stopGraph();
		}
	}

	/**
	 * Stoppt den Graphen
	 */
	private void stopGraph() {
		if (graph != null) {
			graph.Stop();
			MainToNode messeage = new MainToNode(false, null, null,
					null, null, null);
			for (NetChannelOutput out : m_clients.values()) {
				out.write(messeage);
			}
		}
		enabeltForm(true);		
	}
	
	/**
	 * Aktiviert Deaktiviert alle Buttons die nicht betaetigt werden duerfen, wenn
	 * ein Algorithmus ausgefuert wird
	 * @param enabled True wenn Algo nicht leuft
	 */
	private void enabeltForm(boolean enabled){
		timeSlider.setEnabled(enabled);
		restWaitTimeSlider.setEnabled(enabled);
		addNode.setEnabled(enabled);
		editableNodeHandler.setActive(enabled);
		btnEcho.setEnabled(enabled);
		btnElection.setEnabled(enabled);
		btnExit.setEnabled(enabled);
		btnRandom.setEnabled(enabled);
		btnStop.setEnabled(!enabled);
		importButton.setEnabled(enabled);
		exportButton.setEnabled(enabled);
	}
}
