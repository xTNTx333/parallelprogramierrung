var classparallel_programierrung_1_1_nachricht =
[
    [ "MessageType", "enumparallel_programierrung_1_1_nachricht_1_1_message_type.html", "enumparallel_programierrung_1_1_nachricht_1_1_message_type" ],
    [ "Nachricht", "classparallel_programierrung_1_1_nachricht.html#a5b93ef3863cec1cb744b8a1825dc8532", null ],
    [ "getElectionState", "classparallel_programierrung_1_1_nachricht.html#add229b3e9a5f62457d62849dc59dceb7", null ],
    [ "getGraph", "classparallel_programierrung_1_1_nachricht.html#a304f7e8bc31192c574e2cafea0bc4ce2", null ],
    [ "getIDSender", "classparallel_programierrung_1_1_nachricht.html#a5f107e9abbf52831c1306b2a06342d40", null ],
    [ "getLeader", "classparallel_programierrung_1_1_nachricht.html#a175b86fcbe5197bd69342da9ad65982a", null ],
    [ "getMasterID", "classparallel_programierrung_1_1_nachricht.html#af9a34f52b86e8b5cd3695e8bfec0b599", null ],
    [ "getMessageTyp", "classparallel_programierrung_1_1_nachricht.html#ae654c05cacb7f43e730d504f8437fded", null ],
    [ "setElectionState", "classparallel_programierrung_1_1_nachricht.html#afdec0e3a423673b9f4ba2964f7277434", null ],
    [ "setGraph", "classparallel_programierrung_1_1_nachricht.html#abea128e6ba4fc5148b3ea0d569490e27", null ],
    [ "setLeader", "classparallel_programierrung_1_1_nachricht.html#acccbc913a2ab00d37aab04bd4ffdecee", null ]
];