var annotated =
[
    [ "parallelProgramierrung", null, [
      [ "Aufgabe", "enumparallel_programierrung_1_1_aufgabe.html", "enumparallel_programierrung_1_1_aufgabe" ],
      [ "CNSServer", "classparallel_programierrung_1_1_c_n_s_server.html", "classparallel_programierrung_1_1_c_n_s_server" ],
      [ "Echo2", "classparallel_programierrung_1_1_echo2.html", "classparallel_programierrung_1_1_echo2" ],
      [ "ElectionFelix", "classparallel_programierrung_1_1_election_felix.html", "classparallel_programierrung_1_1_election_felix" ],
      [ "ElectionState", "enumparallel_programierrung_1_1_election_state.html", "enumparallel_programierrung_1_1_election_state" ],
      [ "GeneratedGraph", "classparallel_programierrung_1_1_generated_graph.html", "classparallel_programierrung_1_1_generated_graph" ],
      [ "Graphen", "classparallel_programierrung_1_1_graphen.html", "classparallel_programierrung_1_1_graphen" ],
      [ "Helper", "classparallel_programierrung_1_1_helper.html", null ],
      [ "IKnoten", "classparallel_programierrung_1_1_i_knoten.html", "classparallel_programierrung_1_1_i_knoten" ],
      [ "Kante", "classparallel_programierrung_1_1_kante.html", "classparallel_programierrung_1_1_kante" ],
      [ "Nachricht", "classparallel_programierrung_1_1_nachricht.html", "classparallel_programierrung_1_1_nachricht" ],
      [ "ObjectFactory", "classparallel_programierrung_1_1_object_factory.html", "classparallel_programierrung_1_1_object_factory" ]
    ] ]
];