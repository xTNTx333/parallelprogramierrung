var searchData=
[
  ['savegraphen',['SaveGraphen',['../classparallel_programierrung_1_1_helper.html#acfe3460eaa2a45c76d35a46369455321',1,'parallelProgramierrung::Helper']]],
  ['setchannelinput',['setChannelInput',['../classparallel_programierrung_1_1_kante.html#a8038ee82885cc59c70a047711f5379bf',1,'parallelProgramierrung::Kante']]],
  ['setchanneloutput',['setChannelOutput',['../classparallel_programierrung_1_1_kante.html#aa7c4a3d70ef8bdf9b2dcb02b5df98446',1,'parallelProgramierrung::Kante']]],
  ['setelectionstate',['setElectionState',['../classparallel_programierrung_1_1_nachricht.html#afdec0e3a423673b9f4ba2964f7277434',1,'parallelProgramierrung::Nachricht']]],
  ['setgraph',['setGraph',['../classparallel_programierrung_1_1_nachricht.html#abea128e6ba4fc5148b3ea0d569490e27',1,'parallelProgramierrung::Nachricht']]],
  ['setleader',['setLeader',['../classparallel_programierrung_1_1_nachricht.html#acccbc913a2ab00d37aab04bd4ffdecee',1,'parallelProgramierrung::Nachricht']]],
  ['setmessagetime',['setMessageTime',['../classparallel_programierrung_1_1_i_knoten.html#acc0e7627e13f22a7b05d5d1026ea53de',1,'parallelProgramierrung::IKnoten']]],
  ['setoutresultchannel',['setOutResultChannel',['../classparallel_programierrung_1_1_i_knoten.html#a3bdbc7851a1829b8ceb97c398f9cce74',1,'parallelProgramierrung::IKnoten']]],
  ['setsendfirstmessage',['setSendFirstMessage',['../classparallel_programierrung_1_1_kante.html#a7ecd7163eec44d776ed416cdf632aec3',1,'parallelProgramierrung::Kante']]],
  ['setstartnode',['setStartNode',['../classparallel_programierrung_1_1_i_knoten.html#ac7b99a1ee1de69f2a6424f7d45c08835',1,'parallelProgramierrung::IKnoten']]],
  ['setstate',['setState',['../classparallel_programierrung_1_1_echo2.html#adff79062b535b1f0aa57b89b90eaf2df',1,'parallelProgramierrung.Echo2.setState()'],['../classparallel_programierrung_1_1_election_felix.html#a782adeab4924db0ee070a248381e2211',1,'parallelProgramierrung.ElectionFelix.setState()']]],
  ['setthread',['setThread',['../classparallel_programierrung_1_1_generated_graph.html#a0e478fec64180f9000fc6dc6faf457f2',1,'parallelProgramierrung::GeneratedGraph']]],
  ['setwaitforreset',['setWaitForReset',['../classparallel_programierrung_1_1_i_knoten.html#a9d51b48c56033cafb577518b6448b883',1,'parallelProgramierrung::IKnoten']]],
  ['start',['Start',['../classparallel_programierrung_1_1_generated_graph.html#a517948c0c210ba5c0029893385ad4768',1,'parallelProgramierrung::GeneratedGraph']]],
  ['stop',['Stop',['../classparallel_programierrung_1_1_generated_graph.html#a4cb337cc9a8ad43b617a6a6d87b970d8',1,'parallelProgramierrung::GeneratedGraph']]]
];
