var hierarchy =
[
    [ "parallelProgramierrung.Aufgabe", "enumparallel_programierrung_1_1_aufgabe.html", null ],
    [ "parallelProgramierrung.ElectionState", "enumparallel_programierrung_1_1_election_state.html", null ],
    [ "parallelProgramierrung.Helper", "classparallel_programierrung_1_1_helper.html", null ],
    [ "parallelProgramierrung.Kante", "classparallel_programierrung_1_1_kante.html", null ],
    [ "parallelProgramierrung.Nachricht.MessageType", "enumparallel_programierrung_1_1_nachricht_1_1_message_type.html", null ],
    [ "parallelProgramierrung.ObjectFactory", "classparallel_programierrung_1_1_object_factory.html", null ],
    [ "CSProcess", null, [
      [ "parallelProgramierrung.CNSServer", "classparallel_programierrung_1_1_c_n_s_server.html", null ],
      [ "parallelProgramierrung.GeneratedGraph", "classparallel_programierrung_1_1_generated_graph.html", null ],
      [ "parallelProgramierrung.IKnoten", "classparallel_programierrung_1_1_i_knoten.html", [
        [ "parallelProgramierrung.Echo2", "classparallel_programierrung_1_1_echo2.html", null ],
        [ "parallelProgramierrung.ElectionFelix", "classparallel_programierrung_1_1_election_felix.html", null ]
      ] ]
    ] ],
    [ "Serializable", null, [
      [ "parallelProgramierrung.Graphen", "classparallel_programierrung_1_1_graphen.html", null ],
      [ "parallelProgramierrung.Nachricht", "classparallel_programierrung_1_1_nachricht.html", null ]
    ] ]
];